# Natural-User-Interface-experiment

Here is a concept I imagined for an art gallery or any exhibitions. I built the first prototype as my
Bachelor project which was about Natural User Interface. I used Java programming and the Microsoft Kinect sensor. This first version was really heavy because of the many layers, from the sensor management to the adapted multitouch framework.

Mockups & explanation here https://www.behance.net/gallery/19012123/UX-in-Art-Gallery-V1