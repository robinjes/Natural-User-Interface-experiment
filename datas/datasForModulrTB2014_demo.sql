--
-- Dumping data for table `artPiece`
--
INSERT INTO `artPiece` VALUES (1111111111,"Samourai","Anonymous","",29.7,21);

--
-- Dumping data for table `cadre`
--
INSERT INTO `cadre` VALUES (1,20,1200,10,1,1,NULL);

--
-- Dumping data for table `finition`
--
INSERT INTO `finition` VALUES (1111111110,'Blanc',1),(1111111111,'Noir',1.100000023841858),(1111111112,'Brun',1.2000000476837158),(1111111113,'Rouge',1.2999999523162842);

--
-- Dumping data for table `moulure`
--
INSERT INTO `moulure` VALUES (1,'Carre',1.2),(2,'Standard',1),(3,'Louis',1.6),(4,'Dentele',2);

--
-- Dumping data for table `finitionForArtPiece`
--
INSERT INTO `finitionForArtPiece` VALUES (1111111111,1111111111,1);
INSERT INTO `finitionForArtPiece` VALUES (1111111111,1111111113,9);
INSERT INTO `finitionForArtPiece` VALUES (1111111110,1111111113,10);


-- Dump completed on 2014-07-25  9:42:47
