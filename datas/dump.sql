--
-- Table structure for table `artpiece`
--

DROP TABLE IF EXISTS `artpiece`;

CREATE TABLE `artpiece` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(200) NOT NULL,
  `Auteur` varchar(200) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `dimensionVerticaleCm` double NOT NULL,
  `dimensionHorizontaleCm` double NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `cadre`
--

DROP TABLE IF EXISTS `cadre`;

CREATE TABLE `cadre` (
  `ID` int(11) NOT NULL,
  `MinTailleOeuvreCm` double NOT NULL,
  `MaxTailleOeuvreCm` double NOT NULL,
  `PrixCadreBase` double NOT NULL,
  `PrixModuleBase` double NOT NULL,
  `TailleModuleBase` double NOT NULL,
  `artPiece_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `artPiece_ID` (`artPiece_ID`),
  CONSTRAINT `artPiece_ID` FOREIGN KEY (`artPiece_ID`) REFERENCES `artpiece` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `finition`
--

DROP TABLE IF EXISTS `finition`;

CREATE TABLE `finition` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(200) NOT NULL,
  `CoefficientPrix` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `Nom_UNIQUE` (`Nom`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `finitionforartpiece`
--

DROP TABLE IF EXISTS `finitionforartpiece`;

CREATE TABLE `finitionforartpiece` (
  `ArtPiece_ID` int(11) NOT NULL,
  `Finition_ID` int(11) NOT NULL,
  `Priority` int(11) NOT NULL,
  KEY `artPieceLinkFinition_ID` (`ArtPiece_ID`),
  KEY `finition_ID` (`Finition_ID`),
  CONSTRAINT `artPieceLinkFinition_ID` FOREIGN KEY (`ArtPiece_ID`) REFERENCES `artpiece` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `finition_ID` FOREIGN KEY (`Finition_ID`) REFERENCES `finition` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `moulure`
--

DROP TABLE IF EXISTS `moulure`;

CREATE TABLE `moulure` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(200) NOT NULL,
  `coefficientPrix` double NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump completed on 2014-07-25 10:46:50
