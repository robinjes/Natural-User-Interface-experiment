package view;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import org.mt4j.MTApplication;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.input.IMTInputEventListener;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputData.MTInputEvent;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.sceneManagement.transition.FadeTransition;
import org.mt4j.sceneManagement.transition.ITransition;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.MT4jSettings;
import org.mt4j.util.MTColor;
import org.mt4j.util.animation.Animation;
import org.mt4j.util.animation.AnimationEvent;
import org.mt4j.util.animation.IAnimationListener;
import org.mt4j.util.animation.MultiPurposeInterpolator;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.opengl.GLFBO;

import TUIO.TuioClient;
import TUIO.TuioCursor;
import processing.core.PImage;
import tuioProtocol.TuioMouse;
import cursor.MyCursor;
import ch.comem.controleur.GestionnaireDonnees;
import ch.comem.modele.ArtPiece;
import ch.comem.modele.Cadre;

/**
 * @title Interface NUI -TPB2014, Sc�ne de s�lection de l'oeuvre. Cette sc�ne
 *        permet la s�lection d'une oeuvre parmi la gamme propos�e.
 * 
 * @author Robin Jespierre
 *
 */
public class SelectionScene extends AbstractScene {
	/************* Scene properties *************/
	private MTApplication mtapp;
	private AbstractScene dndScene;
	private IFont font;
	private MyCursor cursor;
	private Bridge bridge;
	private ITransition slideLeftTransition;
	private ITransition slideRightTransition;
	/************* Tuio cursor properties *************/
	private MTPolygon caughtComponent;
	private TuioCursor tuioCursor = null;
	private InputCursor cursorL;
	private TuioMouse tuioMouse;
	private Cursor blankCursor;
	private Robot robot = null;
	/************* Art pieces properties *************/
	private ArrayList<ArtPiece> artPieces = null;
	private int nbArtPieces = 0;
	private int maxWidth = 0;
	private int maxHeight = 0;
	private String artPiecePath;
	private MTCarouselMenu menu;
	private int frontalArtPieceId = 1;
	/************* Cadre properties *************/
	private Cadre monCadre;

	/**
	 * Sc�ne de s�lection.
	 * 
	 * @param mtApplication
	 *            L'application de s�lection
	 * @param name
	 *            Le nom de la sc�ne
	 * @param monCadre
	 *            Le cadre pr�sent dans toute l'application
	 * @param bridge
	 *            La classe permettant la liaison entre les diff�rentes couches
	 *            de l'application
	 */
	public SelectionScene(MTApplication mtApplication, String name,
			Cadre monCadre, Bridge bridge) {
		super(mtApplication, name);
		this.mtapp = mtApplication;
		this.bridge = bridge;
		this.dndScene = this;
		this.monCadre = monCadre;
		this.cursor = new MyCursor(new MTColor(0, 0, 0, 0), "cursorIcons\\",
				mtapp, this);
		this.registerGlobalInputProcessor(this.cursor);
		hideMouseCursor();
		initializeArtPieces();
		setSceneProperties();
	}

	/**
	 * Cache le curseur de la souris
	 */
	private void hideMouseCursor() {
//		BufferedImage cursorImg = new BufferedImage(16, 16,
//				BufferedImage.TYPE_INT_ARGB);
//		// Cr�e un nouveau cursor vierge
//		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
//				cursorImg, new Point(0, 0), "blank cursor");
//		this.mtapp.setCursor(blankCursor);
	}

	/**
	 * Initialise les oeuvres d'art � partir de la base de donn�es
	 */
	public void initializeArtPieces() {
		this.artPieces = GestionnaireDonnees.getAllArtPieces();
		this.nbArtPieces = artPieces.size();
		this.maxWidth = (int) (this.mtapp.getWidth() / 3);
		this.maxHeight = this.mtapp.getHeight() / 2;
		this.artPiecePath = "datas//artPieces//";
	}

	/**
	 * Initialise les �l�ment graphiques de la sc�ne
	 */
	public void setSceneProperties() {
		// Disable frustum culling for this scene - optional
		this.getCanvas().setFrustumCulling(false);
		this.setClearColor(MTColor.WHITE);
		int panelWidth = this.mtapp.getWidth();
		int panelHeight = this.mtapp.getHeight();
		/************* Add Canvas Listener to follow the cursor - begin ************/
		// Actions d�finies lors de l'�coute sur le canvas de la sc�ne
		getCanvas().addInputListener(new IMTInputEventListener() {
			public boolean processInputEvent(MTInputEvent inEvt) {
				AbstractCursorInputEvt cursorInputEvt = (AbstractCursorInputEvt) inEvt;
				cursorL = cursorInputEvt.getCursor();
				float x = cursorL.getCurrentEvtPosX();
				float y = cursorL.getCurrentEvtPosY();
				Point cursorPoint = new Point((int) x, (int) y);
				// M�J le curseur de la souris, si pr�sent
				if (tuioCursor == null) {
					tuioCursor = new TuioCursor(5, -1, x, y);
				} else {
					tuioCursor.update(x, y);
					tuioMouse.updateTuioCursor(tuioCursor);
				}
				// Give a feedback if cursor is out of the screen
				isOutOfTheScreen(cursorPoint);
				return false;
			}
		});
		/************* Add Canvas Listener to follow the cursor - end ************/
		/***** Layer effect properties - begin *****/
		float upperBarreWidth1 = panelWidth;
		float upperBarreHeight1 = 20;
		float upperBarreXPosition1 = 0;
		float upperBarreYPosition1 = 0;
		MTRectangle upperBarre1 = new MTRectangle(upperBarreXPosition1,
				upperBarreYPosition1, upperBarreWidth1, upperBarreHeight1,
				this.mtapp);
		upperBarre1.removeAllGestureEventListeners();
		upperBarre1.setNoStroke(true);
		upperBarre1.setFillColor(new MTColor(235, 235, 235));
		upperBarre1.setName("Upper barre");
		this.getCanvas().addChild(upperBarre1);

		float rightBarreWidth1 = 20;
		float rightBarreHeight1 = panelHeight;
		float rightBarreXPosition1 = panelWidth - rightBarreWidth1;
		float rightBarreYPosition1 = 0;
		MTRectangle rightBarre1 = new MTRectangle(rightBarreXPosition1,
				rightBarreYPosition1, rightBarreWidth1, rightBarreHeight1,
				this.mtapp);
		rightBarre1.removeAllGestureEventListeners();
		rightBarre1.setNoStroke(true);
		rightBarre1.setFillColor(new MTColor(235, 235, 235));
		rightBarre1.setName("Upper barre");
		this.getCanvas().addChild(rightBarre1);

		panelHeight = (int) (panelHeight - upperBarreHeight1);
		panelWidth = (int) (panelWidth - rightBarreWidth1);
		/***** Layer effect properties - end *****/
		/***** Close button properties - begin *****/
		float closeImageWidth = 70;
		float closeImageHeight = 70;
		float closeImageXPosition = this.mtapp.getWidth() - rightBarreWidth1
				- closeImageWidth - 20;
		float closeImageYPosition = upperBarreHeight1 + 20;
		MTRectangle closeImagePanel = new MTRectangle(closeImageXPosition,
				closeImageYPosition, closeImageWidth, closeImageHeight,
				this.mtapp);
		closeImagePanel.removeAllGestureEventListeners();
		closeImagePanel.setNoStroke(true);
		closeImagePanel.setFillColor(new MTColor(250, 250, 250));
		closeImagePanel.setName("Close image panel");
		PImage closeImg = this.mtapp
				.loadImage("ressourcesGraphiquesNUI\\closeCross.png");
		closeImagePanel.setTexture(closeImg);
		this.getCanvas().addChild(closeImagePanel);
		/***** Close button properties - end *****/
		/***** Close zone properties - begin *****/
		float closeZoneWidth = 200;
		float closeZoneHeight = 200;
		float closeZoneXPosition = this.mtapp.getWidth() - closeZoneWidth;
		float closeZoneYPosition = 0;
		MTRectangle closeZone = new MTRectangle(closeZoneXPosition,
				closeZoneYPosition, closeZoneWidth, closeZoneHeight, this.mtapp);
		closeZone.removeAllGestureEventListeners();
		// closeZone.setNoStroke(true);
		closeZone.setStrokeColor(MTColor.BLUE);
		closeZone.setFillColor(new MTColor(0, 0, 0, 0));
		closeZone.setName("Close zone");
		this.getCanvas().addChild(closeZone);
		TapAndHoldProcessor tapAndHold = new TapAndHoldProcessor(mtapp);
		tapAndHold.setHoldTime(bridge.getSlowTapTime());
		closeZone.registerInputProcessor(tapAndHold);
		closeZone.addGestureListener(TapAndHoldProcessor.class,
				new TapAndHoldVisualizer(this.mtapp, getCanvas()));
		closeZone.addGestureListener(TapAndHoldProcessor.class,
				new IGestureEventListener() {
					public boolean processGestureEvent(MTGestureEvent ge) {
						TapAndHoldEvent t = (TapAndHoldEvent) ge;
						MTPolygon target = (MTPolygon) ge.getCurrentTarget();
						if (t.isHoldComplete()) {
							setTransition(slideRightTransition);
							mtapp.changeScene(mtapp.getScene("Home"));
							bridge.cursorRelease();
						}
						return false;
					}
				});
		/***** Close zone properties - end *****/
		/***** OpenGl activation check for transition - begin *****/
		if (MT4jSettings.getInstance().isOpenGlMode()
				&& GLFBO.isSupported(this.mtapp)) {
			slideLeftTransition = new SlideTransition(this.mtapp, 700, true);
			slideRightTransition = new SlideTransition(this.mtapp, 700, false);
		} else {
			this.setTransition(new FadeTransition(this.mtapp));
		}
		/***** OpenGl activation check for transition - end *****/
		/***** Carousel Menu properties - begin *****/
		// count number of art pieces (8 by default)
		menu = new MTCarouselMenu(
				mtapp,
				new Vector3D(
						mtapp.width / 2,
						(float) (((mtapp.height / 1.8)) - (this.maxHeight / 2)),
						-1000), this.nbArtPieces, 5 * this.maxWidth
						/ this.nbArtPieces);
		getCanvas().addChild(menu);
		MTRectangle[] carouselItems = menu.carouselItems;
		int i = 0;
		for (MTRectangle carouselItem : carouselItems) {
			if (i < this.nbArtPieces) {
				carouselItem.setWidthLocal(50);
				carouselItem.setHeightLocal(50);
				addArtPiece(carouselItem, artPieces.get(i), i);
				carouselItem.setNoStroke(true);
				carouselItem.setNoFill(true);
				carouselItem.setPickable(false);
			}
			i++;
		}
		/***** Carousel Menu properties - end *****/
		/************* Initialize cursor click fonction - begin ************/
		int port = 3333;
		tuioMouse = new TuioMouse();
		TuioClient client = new TuioClient(port);
		client.addTuioListener(tuioMouse);
		/************* Initialize cursor click fonction - end ************/
	}

	/**
	 * Fonction permettant l'affichage d'un feedback visuel lorsque le curseur
	 * de la main se trouve hors de l'�cran
	 * 
	 * @param cursorPoint
	 *            Point 2D du curseur
	 */
	private void isOutOfTheScreen(Point cursorPoint) {
		float x = cursorPoint.x;
		float y = cursorPoint.y;
		final MTRectangle outOfScreenIcon_bottom = new MTRectangle(10, 10, 50,
				50, this.mtapp);
		PImage outOfScreenImg_bottom = this.mtapp
				.loadImage("cursorIcons\\outOfTheScreen_bottom.png");
		outOfScreenIcon_bottom.setTexture(outOfScreenImg_bottom);
		outOfScreenIcon_bottom.setNoFill(true);
		outOfScreenIcon_bottom.setNoStroke(true);
		outOfScreenIcon_bottom.setVisible(false);
		this.getCanvas().addChild(outOfScreenIcon_bottom);
		final MTRectangle outOfScreenIcon_left = new MTRectangle(10, 10, 50,
				50, this.mtapp);
		PImage outOfScreenImg_left = this.mtapp
				.loadImage("cursorIcons\\outOfTheScreen_left.png");
		outOfScreenIcon_left.setTexture(outOfScreenImg_left);
		outOfScreenIcon_left.setNoFill(true);
		outOfScreenIcon_left.setNoStroke(true);
		outOfScreenIcon_left.setVisible(false);
		this.getCanvas().addChild(outOfScreenIcon_left);
		final MTRectangle outOfScreenIcon_top = new MTRectangle(10, 10, 50, 50,
				this.mtapp);
		PImage outOfScreenImg_top = this.mtapp
				.loadImage("cursorIcons\\outOfTheScreen_top.png");
		outOfScreenIcon_top.setTexture(outOfScreenImg_top);
		outOfScreenIcon_top.setNoFill(true);
		outOfScreenIcon_top.setNoStroke(true);
		outOfScreenIcon_top.setVisible(false);
		this.getCanvas().addChild(outOfScreenIcon_top);
		final MTRectangle outOfScreenIcon_right = new MTRectangle(10, 10, 50,
				50, this.mtapp);
		PImage outOfScreenImg_right = this.mtapp
				.loadImage("cursorIcons\\outOfTheScreen_right.png");
		outOfScreenIcon_right.setTexture(outOfScreenImg_right);
		outOfScreenIcon_right.setNoFill(true);
		outOfScreenIcon_right.setNoStroke(true);
		outOfScreenIcon_right.setVisible(false);
		this.getCanvas().addChild(outOfScreenIcon_right);
		if (!mtapp.getBounds().contains(cursorPoint)) {
			float outOfScreenIconPositionX = 0;
			float outOfScreenIconPositionY = 0;
			Vector3D outOfScreenIconGlobalPosition = null;
			System.out.println("Cursor is out of the screen!!");
			if (x > mtapp.getWidth()) {
				// on right
				outOfScreenIconPositionX = mtapp.getWidth() - 70;
				outOfScreenIconPositionY = y;
				outOfScreenIconGlobalPosition = new Vector3D(
						outOfScreenIconPositionX, outOfScreenIconPositionY);
				outOfScreenIcon_right
						.setPositionGlobal(outOfScreenIconGlobalPosition);
				setAllVisibleFalse(outOfScreenIcon_bottom,
						outOfScreenIcon_left, outOfScreenIcon_right,
						outOfScreenIcon_top);
				outOfScreenIcon_right.setVisible(true);
			} else if (x < 0) {
				// on left
				outOfScreenIconPositionX = 70;
				outOfScreenIconPositionY = y;
				outOfScreenIconGlobalPosition = new Vector3D(
						outOfScreenIconPositionX, outOfScreenIconPositionY);
				outOfScreenIcon_left
						.setPositionGlobal(outOfScreenIconGlobalPosition);
				setAllVisibleFalse(outOfScreenIcon_bottom,
						outOfScreenIcon_left, outOfScreenIcon_right,
						outOfScreenIcon_top);
				outOfScreenIcon_left.setVisible(true);
			} else if (y > mtapp.getHeight()) {
				// on bottom
				outOfScreenIconPositionX = x;
				outOfScreenIconPositionY = mtapp.getHeight() - 70;
				outOfScreenIconGlobalPosition = new Vector3D(
						outOfScreenIconPositionX, outOfScreenIconPositionY);
				outOfScreenIcon_bottom
						.setPositionGlobal(outOfScreenIconGlobalPosition);
				setAllVisibleFalse(outOfScreenIcon_bottom,
						outOfScreenIcon_left, outOfScreenIcon_right,
						outOfScreenIcon_top);
				outOfScreenIcon_bottom.setVisible(true);
			} else {
				// on top
				outOfScreenIconPositionX = x;
				outOfScreenIconPositionY = 70;
				outOfScreenIconGlobalPosition = new Vector3D(
						outOfScreenIconPositionX, outOfScreenIconPositionY);
				outOfScreenIcon_top
						.setPositionGlobal(outOfScreenIconGlobalPosition);
				setAllVisibleFalse(outOfScreenIcon_bottom,
						outOfScreenIcon_left, outOfScreenIcon_right,
						outOfScreenIcon_top);
				outOfScreenIcon_top.setVisible(true);
			}
		} else {
			setAllVisibleFalse(outOfScreenIcon_bottom, outOfScreenIcon_left,
					outOfScreenIcon_right, outOfScreenIcon_top);
		}
	}

	/**
	 * Cache toutes les images du feedback visuel indiquant que le curseur se
	 * trouve en dehors de l'�cran
	 * 
	 * @param outOfScreenIcon_bottom
	 *            Feedback visuel bas
	 * @param outOfScreenIcon_left
	 *            Feedback visuel gauche
	 * @param outOfScreenIcon_right
	 *            Feedback visuel droite
	 * @param outOfScreenIcon_top
	 *            Feedback visuel haut
	 */
	private void setAllVisibleFalse(MTRectangle outOfScreenIcon_bottom,
			MTRectangle outOfScreenIcon_left,
			MTRectangle outOfScreenIcon_right, MTRectangle outOfScreenIcon_top) {
		outOfScreenIcon_bottom.setVisible(false);
		outOfScreenIcon_left.setVisible(false);
		outOfScreenIcon_right.setVisible(false);
		outOfScreenIcon_top.setVisible(false);
	}

	/**
	 * Ajoute l'oeuvre d'art au carousel de la sc�ne.
	 * 
	 * @param item
	 *            El�ment graphique sur lequel repose l'oeuvre
	 * @param image
	 *            L'image de l'oeuvre
	 * @param i
	 *            Son emplacement dans le carousel
	 */
	public void addArtPiece(MTRectangle item, ArtPiece image, int i) {
		double horizontalDimension = image.getHorizontalDimension();
		double verticalDimension = image.getVerticalDimension();
		int width = this.maxWidth;
		int height = this.maxWidth;
		final float x = item.getCenterPointLocal().x - width / 2;
		final float y = item.getCenterPointLocal().y;

		if (horizontalDimension <= verticalDimension) {
			float ratio = (float) (verticalDimension / horizontalDimension);
			height = this.maxHeight;
			width = (int) (this.maxHeight / ratio);
		} else if (horizontalDimension > verticalDimension) {
			float ratio = (float) (horizontalDimension / verticalDimension);
			width = this.maxWidth;
			height = (int) (this.maxWidth / ratio);
		}
		MTRectangle artPieceImg = new MTRectangle(x, y, width, height,
				this.mtapp);
		PImage pImg = mtapp.loadImage(this.artPiecePath + image.getName()
				+ ".jpg");
		artPieceImg.setTexture(pImg);
		TapAndHoldProcessor tapAndHoldArtPiece = new TapAndHoldProcessor(mtapp);
		tapAndHoldArtPiece.setHoldTime(bridge.getSlowTapTime());
		artPieceImg.registerInputProcessor(tapAndHoldArtPiece);
		activeSelectionOnTheFrontArtPiece(artPieceImg, artPieceImg, i);
		item.setPickable(false);
		item.addChild(artPieceImg);
	}

	/**
	 * Fait tourner le carousel d'un cran vers la droite
	 */
	public void carouselTurnRight() {
		MultiPurposeInterpolator interpolator = new MultiPurposeInterpolator(0,
				0, 250, 0, 1000, 1);
		Animation eastAnimation = new Animation("east animation", interpolator,
				this);
		eastAnimation.addAnimationListener(new IAnimationListener() {
			@Override
			public void processAnimationEvent(AnimationEvent ae) {
				menu.rotateY(menu.getCenterPointGlobal(), (float) 2.82);
			}
		});
		eastAnimation.start();

		MTRectangle oldItem = (MTRectangle) menu.carouselItems[this.frontalArtPieceId]
				.getChildren()[0];
		if (this.frontalArtPieceId == nbArtPieces - 1) {
			this.frontalArtPieceId = 0;
		} else {
			this.frontalArtPieceId++;
		}
		MTRectangle newItem = (MTRectangle) menu.carouselItems[this.frontalArtPieceId]
				.getChildren()[0];
		activeSelectionOnTheFrontArtPiece(oldItem, newItem,
				this.frontalArtPieceId);
	}

	/**
	 * Fait tourner le carousel d'un cran vers la gauche
	 */
	public void carouselTurnLeft() {
		MultiPurposeInterpolator westInterpolator = new MultiPurposeInterpolator(
				0, 0, 250, 0, 1000, 1);
		Animation westAnimation = new Animation("west animation",
				westInterpolator, this);
		westAnimation.addAnimationListener(new IAnimationListener() {
			@Override
			public void processAnimationEvent(AnimationEvent ae) {
				menu.rotateY(menu.getCenterPointGlobal(), (float) -2.82);
			}
		});
		westAnimation.start();

		MTRectangle oldItem = (MTRectangle) menu.carouselItems[this.frontalArtPieceId]
				.getChildren()[0];
		if (this.frontalArtPieceId == 0) {
			this.frontalArtPieceId = nbArtPieces - 1;
		} else {
			this.frontalArtPieceId--;
		}
		MTRectangle newItem = (MTRectangle) menu.carouselItems[this.frontalArtPieceId]
				.getChildren()[0];
		activeSelectionOnTheFrontArtPiece(oldItem, newItem,
				this.frontalArtPieceId);
	}

	/**
	 * Rend actif la s�lection de l'oeuvre sur laquelle on se trouve et
	 * d�sactive les autres
	 * 
	 * @param oldItem
	 *            L'oeuvre pr�c�dente
	 * @param newItem
	 *            La nouvelle oeuvre
	 * @param newItemId
	 *            L'emplacement de la nouvelle oeuvre dans le carousel
	 */
	private void activeSelectionOnTheFrontArtPiece(MTRectangle oldItem,
			MTRectangle newItem, int newItemId) {
		final MTRectangle oldOne = oldItem;
		final MTRectangle newOne = newItem;
		final int itemId = newItemId;
		// oldOne.removeAllGestureEventListeners();
		final Vector3D coords = newOne.getCenterPointRelativeToParent();
		newOne.addGestureListener(TapAndHoldProcessor.class,
				new TapAndHoldVisualizer(this.mtapp, getCanvas()));
		newOne.addGestureListener(TapAndHoldProcessor.class,
				new IGestureEventListener() {
					@Override
					public boolean processGestureEvent(MTGestureEvent ge) {
						TapAndHoldEvent t = (TapAndHoldEvent) ge;
						if (t.isHoldComplete()) {
							MainScene mainScene = (MainScene) mtapp
									.getScene("Main");
							mainScene.initializeCadre(artPieces.get(itemId)
									.getVerticalDimension(),
									artPieces.get(itemId)
											.getHorizontalDimension(), newOne);
							try {
								monCadre.setImg((BufferedImage) newOne
										.getTexture().getImage());
							} catch (IOException e) {
								System.out
										.println("Image de l'oeuvre FAILLLLLLLED");
								e.printStackTrace();
							}
							;
							mtapp.changeScene(mtapp.getScene("Main"));
							bridge.cursorRelease();
						}
						return false;
					}
				});
		newOne.addInputListener(new IMTInputEventListener() {
			public boolean processInputEvent(MTInputEvent inEvt) {
				newOne.setPositionRelativeToParent(coords);
				return false;
			}
		});
		float iconX = (float) (newOne.getCenterPointLocal().x + (artPieces.get(
				itemId).getHorizontalDimension() * 2));
		float iconY = (float) (newOne.getCenterPointLocal().y);
		float iconWidth = 100;
		float iconHeight = 100;
		MTRectangle selectableIcon = new MTRectangle(iconX, iconY, iconWidth,
				iconHeight, this.mtapp);
		selectableIcon.setName("selectableIcon");
		selectableIcon.setNoFill(true);
		selectableIcon.setNoStroke(true);
		selectableIcon.setPickable(false);
		PImage icon = this.mtapp
				.loadImage("cursorIcons\\zoneSelectionnable.png");
		selectableIcon.setTexture(icon);
		newOne.addChild(selectableIcon);
	}

	/**
	 * Renvoie le curseur de la sc�ne
	 * 
	 * @return InputCursor
	 */
	public InputCursor getInputCursor() {
		return this.cursorL;
	}

	/**
	 * Renvoie l'objet TuioMouse
	 * 
	 * @return TuioMouse
	 */
	public TuioMouse getTuioMouse() {
		return this.tuioMouse;
	}

	/**
	 * Renvoie l'objet TuioCursor
	 * 
	 * @return TuioCursor
	 */
	public TuioCursor getTuioCursor() {
		return this.tuioCursor;
	}
}
