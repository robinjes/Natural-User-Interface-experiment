package view;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import org.mt4j.MTApplication;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.components.visibleComponents.widgets.MTTextArea;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.util.MTColor;
import org.mt4j.util.font.FontManager;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.Vector3D;

import processing.core.PImage;
import ch.comem.modele.Cadre;

/**
 * @title Interface NUI -TPB2014, Sc�ne d'accueil. Cette sc�ne affiche un cadre
 *        bariol� et invite l'utiliseur � faire un signe de la main.
 * 
 * @author Robin Jespierre
 *
 */
public class HomeScene extends AbstractScene {
	/************* Scene properties *************/
	private MTApplication mtapp;

	/**
	 * Sc�ne d'accueil.
	 * 
	 * @param mtApplication
	 *            L'application principale
	 * @param name
	 *            Le nom de la sc�ne
	 * @param monCadre
	 *            Le cadre pr�sent dans toute l'application
	 * @param bridge
	 *            La classe permettant la liaison entre les diff�rentes couches
	 *            de l'application
	 */
	public HomeScene(MTApplication mtApplication, String name, Cadre monCadre,
			Bridge bridge) {
		super(mtApplication, name);
		this.mtapp = mtApplication;
		hideMouseCursor();
		setSceneProperties();
	}

	/**
	 * Cache le curseur de la souris
	 */
	private void hideMouseCursor() {
		BufferedImage cursorImg = new BufferedImage(16, 16,
				BufferedImage.TYPE_INT_ARGB);
		// Cr�e un nouveau cursor vierge
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				cursorImg, new Point(0, 0), "blank cursor");
		this.mtapp.setCursor(blankCursor);
	}

	/**
	 * Initialise les �l�ment graphiques de la sc�ne
	 */
	public void setSceneProperties() {
		this.getCanvas().setFrustumCulling(false);
		this.setClearColor(MTColor.WHITE);
		int panelWidth = this.mtapp.getWidth();
		int panelHeight = this.mtapp.getHeight();
		/***** Cadre Panel properties - begin *****/
		float cadrePanelWidth = panelWidth;
		float cadrePanelHeight = panelHeight / 2;
		float cadrePanelXPosition = 0;
		float cadrePanelYposition = panelHeight / 5;
		MTRectangle cadrePanel = new MTRectangle(cadrePanelXPosition,
				cadrePanelYposition, cadrePanelWidth, cadrePanelHeight,
				this.mtapp);
		cadrePanel.removeAllGestureEventListeners();
		cadrePanel.setNoStroke(true);
		cadrePanel.setFillColor(new MTColor(236, 240, 241));
		cadrePanel.setName("Cadre Panel");
		this.getCanvas().addChild(cadrePanel);
		/***** Cadre Panel properties - end *****/
		/***** Title properties - begin *****/
		IFont titleFont = FontManager.getInstance().createFont(this.mtapp,
				"arial.ttf", 90, // Font size
				new MTColor(80, 80, 80, 255));
		MTTextArea title = new MTTextArea(this.mtapp, titleFont);
		title.setNoStroke(true);
		title.setNoFill(true);
		title.setText("Bienvenue");
		title.setPositionGlobal(new Vector3D(panelWidth / 2f, 120));
		this.getCanvas().addChild(title);
		/***** Title properties - end *****/
		/***** Wave Picture properties - begin *****/
		float waveImageWidth = (float) 281.33;
		float waveImageHeight = (float) 198.33;
		float waveImageXPosition = (panelWidth / 2f) - waveImageWidth / 2;
		float waveImageYPosition = panelHeight - waveImageHeight - 120;
		MTRectangle waveImagePanel = new MTRectangle(waveImageXPosition,
				waveImageYPosition, waveImageWidth, waveImageHeight, this.mtapp);
		waveImagePanel.removeAllGestureEventListeners();
		waveImagePanel.setNoStroke(true);
		waveImagePanel.setFillColor(new MTColor(250, 250, 250));
		waveImagePanel.setName("Wave image panel");
		PImage waveImg = this.mtapp
				.loadImage("ressourcesGraphiquesNUI\\wave.png");
		waveImagePanel.setTexture(waveImg);
		this.getCanvas().addChild(waveImagePanel);
		/***** Wave Picture properties - end *****/
		/***** Instruction properties - begin *****/
		IFont instructionFont = FontManager.getInstance().createFont(
				this.mtapp, "arial.ttf", 30, // Font size
				new MTColor(80, 80, 80, 200));
		MTTextArea underTitle = new MTTextArea(this.mtapp, instructionFont);
		underTitle.setNoStroke(true);
		underTitle.setNoFill(true);
		underTitle.setText("Saluez l'�cran avec votre main...");
		underTitle.setPositionGlobal(new Vector3D(panelWidth / 2f,
				panelHeight - 90));
		this.getCanvas().addChild(underTitle);
		/***** Instruction properties - end *****/
	}
}
