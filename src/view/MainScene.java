package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.OpenNI.Point3D;
import org.mt4j.MTApplication;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.input.IMTInputEventListener;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputData.MTInputEvent;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.sceneManagement.transition.FadeTransition;
import org.mt4j.sceneManagement.transition.ITransition;
import org.mt4j.sceneManagement.transition.SlideTransition;
import org.mt4j.util.MT4jSettings;
import org.mt4j.util.MTColor;
import org.mt4j.util.font.FontManager;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.Vector3D;
import org.mt4j.util.math.Vertex;
import org.mt4j.util.opengl.GLFBO;

import processing.core.PImage;
import tuioProtocol.TuioMouse;
import TUIO.TuioClient;
import TUIO.TuioCursor;
import cursor.MyCursor;
import ch.comem.controleur.GestionnaireDonnees;
import ch.comem.modele.Angle;
import ch.comem.modele.ArtPiece;
import ch.comem.modele.Cadre;
import ch.comem.modele.Finition;
import ch.comem.modele.Module;
import dNd_action.AbstractDnDAction;
import dNd_action.DragAndDropAction;
import dNd_action.DropTarget;
import dNd_action.MyDragAndDropTarget;

/**
 * @title Interface NUI -TPB2014, Sc�ne principale. Cette sc�ne permet la
 *        personnalisation d'un cadre autour d'une oeuvre s�lectionn�e.
 * 
 * @author Robin Jespierre
 *
 */
public class MainScene extends AbstractScene {
	/************* Scene properties *************/
	private MTApplication mtapp;
	private AbstractScene dndScene;
	private IFont font;
	private MyCursor cursor;
	private Bridge bridge;
	private ITransition slideLeftTransition;
	private ITransition slideRightTransition;
	private float actualPanelWidth;
	private float actualPanelHeight;
	/************* Cadre properties *************/
	private Cadre monCadre;
	final double mtb = 1; // module taille base
	final double atb = 14.5; // angle taille base
	final int pw = 5; // profile width
	private double moduleTailleBase = 0;
	private double angleTailleBase = 0;
	private int profileWidth = 0;
	/*******/
	private int topSideSize;
	private int rightSideSize;
	private int bottomSideSize;
	private int leftSideSize;
	/*******/
	private int departCadreX = 0;
	private int departCadreY = 0;
	/*******/
	private int draggableElementEnlargement = 2; // Draggable elements are twice
													// the original size
	/*******/
	private ArrayList<Module> modulesType;
	/*******/
	private ArrayList<Finition> finitions = null;
	/************* Art document properties *************/
	private double verticalSize;
	private double horizontalSize;
	/************* DnD properties *************/
	private Map<String, MTColor> dndComponents;
	private float dragAndDropPanel_height;
	private float dragAndDropPanel_width;
	private float dragAndDropPanel_positionX;
	private float dragAndDropPanel_positionY;
	private Module smallModule;
	private Module middleModule;
	private Module bigModule;
	private Angle deck;
	/************* Menu finitions properties *************/
	private MTRectangle roundFinitionMenu;
	private MTCarouselMenu menu;
	private int nbFinitions = 1;
	/************* Tuio properties *************/
	private TuioCursor tuioCursor = null;
	private InputCursor cursorL;
	private TuioMouse tuioMouse;

	/**
	 * Sc�ne principale.
	 * 
	 * @param mtApplication
	 *            L'application principale
	 * @param name
	 *            Le nom de la sc�ne
	 * @param monCadre
	 *            Le cadre pr�sent dans toute l'application
	 * @param bridge
	 *            La classe permettant la liaison entre les diff�rentes couches
	 *            de l'application
	 */
	public MainScene(MTApplication mtApplication, String name, Cadre monCadre,
			Bridge bridge) {
		super(mtApplication, name);
		this.mtapp = mtApplication;
		this.bridge = bridge;
		this.dndScene = this;
		this.monCadre = monCadre;
		this.cursor = new MyCursor(new MTColor(0, 0, 0, 0), "cursorIcons\\",
				mtapp, this);
		this.registerGlobalInputProcessor(this.cursor);
		hideMouseCursor();
	}

	/**
	 * Cache le curseur de la souris
	 */
	private void hideMouseCursor() {
		BufferedImage cursorImg = new BufferedImage(16, 16,
				BufferedImage.TYPE_INT_ARGB);
		// Cr�e un nouveau cursor vierge
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				cursorImg, new Point(0, 0), "blank cursor");
		this.mtapp.setCursor(blankCursor);
	}

	/**
	 * Initialise toute la sc�ne � partir de l'oeuvre d'art s�lectionn�e.
	 * 
	 * @param verticalSizeArtPiece
	 *            Dimension verticale de l'oeuvre
	 * @param horizontalSizeArtPiece
	 *            Dimension horizontale de l'oeuvre
	 * @param artPieceImg
	 *            Image de l'oeuvre
	 */
	public void initializeCadre(double verticalSizeArtPiece,
			double horizontalSizeArtPiece, MTRectangle artPieceImg) {
		setCadreProperties(verticalSizeArtPiece, horizontalSizeArtPiece);
		setSceneProperties();
		displayDefaultCadre();
		diplayCadreElementsToDrag();
		displayArtPiece(artPieceImg);
	}

	/**
	 * Initialise les �l�ment graphiques de la sc�ne
	 */
	public void setSceneProperties() {
		// Disable frustum culling for this scene - optional
		this.getCanvas().setFrustumCulling(false);
		this.setClearColor(new MTColor(146, 150, 100, 255));
		this.font = FontManager.getInstance().createFont(this.mtapp,
				"arial.ttf", 30, // Font size
				new MTColor(0, 0, 0, 255), // Font fill color
				new MTColor(0, 0, 0, 255));
		actualPanelWidth = this.mtapp.getWidth();
		actualPanelHeight = this.mtapp.getHeight();
		/************* Add Canvas Listener to follow the cursor - begin ************/
		getCanvas().addInputListener(new IMTInputEventListener() {
			public boolean processInputEvent(MTInputEvent inEvt) {
				AbstractCursorInputEvt cursorInputEvt = (AbstractCursorInputEvt) inEvt;
				cursorL = cursorInputEvt.getCursor();
				float x = cursorL.getCurrentEvtPosX();
				float y = cursorL.getCurrentEvtPosY();
				if (tuioCursor == null) {
					tuioCursor = new TuioCursor(5, -1, x, y);
				} else {
					tuioCursor.update(x, y);
					tuioMouse.updateTuioCursor(tuioCursor);
				}
				return false;
			}
		});
		/************* Add Canvas Listener to follow the cursor - end ************/
		/***** OpenGl activation check for transition - begin *****/
		if (MT4jSettings.getInstance().isOpenGlMode()
				&& GLFBO.isSupported(this.mtapp)) {
			slideLeftTransition = new SlideTransition(this.mtapp, 700, true);
			slideRightTransition = new SlideTransition(this.mtapp, 700, false);
		} else {
			this.setTransition(new FadeTransition(this.mtapp));
		}
		/***** OpenGl activation check for transition - end *****/
		/***** Layer effect properties - begin *****/
		float backgroundWidth1 = this.mtapp.getWidth();
		float backgroundHeight1 = this.mtapp.getHeight();
		float backgroundXPosition1 = 0;
		float backgroundYPosition1 = 0;
		MTRectangle background1 = new MTRectangle(backgroundXPosition1,
				backgroundYPosition1, backgroundWidth1, backgroundHeight1,
				this.mtapp);
		background1.removeAllGestureEventListeners();
		background1.setStrokeColor(new MTColor(80, 80, 80));
		background1.setFillColor(new MTColor(178, 178, 178));
		background1.setName("Background 1");
		this.getCanvas().addChild(background1);

		float backgroundWidth2 = this.mtapp.getWidth();
		float backgroundHeight2 = this.mtapp.getHeight();
		float backgroundXPosition2 = -10;
		float backgroundYPosition2 = 10;
		MTRectangle background2 = new MTRectangle(backgroundXPosition2,
				backgroundYPosition2, backgroundWidth2, backgroundHeight2,
				this.mtapp);
		background2.removeAllGestureEventListeners();
		background2.setStrokeColor(new MTColor(80, 80, 80));
		background2.setFillColor(new MTColor(204, 204, 204));
		background2.setName("Background 1");
		this.getCanvas().addChild(background2);
		/***** Cadre Panel properties *****/
		float cadrePanel_width = this.mtapp.getWidth();
		float cadrePanel_height = this.mtapp.getHeight();
		float cadrePanel_positionX = -20;
		float cadrePanel_positionY = 20;
		MTRectangle cadrePanel = new MTRectangle(cadrePanel_positionX,
				cadrePanel_positionY, cadrePanel_width, cadrePanel_height,
				this.mtapp);
		cadrePanel.removeAllGestureEventListeners();
		cadrePanel.setStrokeColor(new MTColor(80, 80, 80));
		cadrePanel.setFillColor(new MTColor(226, 226, 226));
		cadrePanel.setName("Cadre Panel");
		this.getCanvas().addChild(cadrePanel);
		/***** Modules menu properties *****/
		this.dragAndDropPanel_width = 400;
		this.dragAndDropPanel_height = this.mtapp.getHeight();
		this.dragAndDropPanel_positionX = this.mtapp.getWidth()
				- dragAndDropPanel_width - 20;
		this.dragAndDropPanel_positionY = 20;

		MTRectangle dragAndDropPanel = new MTRectangle(
				dragAndDropPanel_positionX, dragAndDropPanel_positionY,
				dragAndDropPanel_width, dragAndDropPanel_height, this.mtapp);
		dragAndDropPanel.removeAllGestureEventListeners();
		dragAndDropPanel.setNoStroke(true);
		dragAndDropPanel.setFillColor(new MTColor(128, 128, 128));
		dragAndDropPanel.setName("Modules menu");
		this.getCanvas().addChild(dragAndDropPanel);
		/***** Close button properties - begin *****/
		float closeImageWidth = 70;
		float closeImageHeight = 70;
		float closeImageXPosition = this.mtapp.getWidth() - closeImageWidth
				- 40;
		float closeImageYPosition = 40;
		MTRectangle closeImagePanel = new MTRectangle(closeImageXPosition,
				closeImageYPosition, closeImageWidth, closeImageHeight,
				this.mtapp);
		closeImagePanel.removeAllGestureEventListeners();
		closeImagePanel.setNoStroke(true);
		closeImagePanel.setFillColor(new MTColor(250, 250, 250));
		closeImagePanel.setName("Close image panel");
		PImage closeImg = this.mtapp
				.loadImage("ressourcesGraphiquesNUI\\closeCross.png");
		closeImagePanel.setTexture(closeImg);
		this.getCanvas().addChild(closeImagePanel);
		/***** Close button properties - end *****/
		/***** Close zone properties - begin *****/
		float closeZoneWidth = this.dragAndDropPanel_width;
		float closeZoneHeight = 130;
		float closeZoneXPosition = this.mtapp.getWidth() - closeZoneWidth;
		float closeZoneYPosition = 0;
		MTRectangle closeZone = new MTRectangle(closeZoneXPosition,
				closeZoneYPosition, closeZoneWidth, closeZoneHeight, this.mtapp);
		closeZone.removeAllGestureEventListeners();
		// closeZone.setNoStroke(true);
		closeZone.setStrokeColor(MTColor.BLUE);
		closeZone.setFillColor(new MTColor(0, 0, 0, 0));
		closeZone.setName("Close zone");
		this.getCanvas().addChild(closeZone);
		TapAndHoldProcessor tapAndHold = new TapAndHoldProcessor(mtapp);
		tapAndHold.setHoldTime(bridge.getSlowTapTime());
		closeZone.registerInputProcessor(tapAndHold);
		closeZone.addGestureListener(TapAndHoldProcessor.class,
				new TapAndHoldVisualizer(this.mtapp, getCanvas()));
		closeZone.addGestureListener(TapAndHoldProcessor.class,
				new IGestureEventListener() {
					public boolean processGestureEvent(MTGestureEvent ge) {
						TapAndHoldEvent t = (TapAndHoldEvent) ge;
						MTPolygon target = (MTPolygon) ge.getCurrentTarget();
						if (t.isHoldComplete()) {
							setTransition(slideRightTransition);
							mtapp.changeScene(mtapp.getScene("Selection"));
						}
						return false;
					}
				});
		/***** Close zone properties - end *****/
		/***** Finitions carousel properties - begin *****/
		/***** Carousel Menu properties - begin *****/
		// MTCarouselMenu(MTApplication mtapp, Vector3D centerPoint, int
		// segments, int elipseWidth)
		Vector3D carouselCenterPoint = new Vector3D(
				((this.mtapp.getWidth() - this.dragAndDropPanel_width) / 2),
				this.mtapp.getHeight() - 300, 0);
		menu = new MTCarouselMenu(mtapp, carouselCenterPoint,
				this.nbFinitions * 2, 400);
		getCanvas().addChild(menu);
		MTRectangle[] carouselItems = menu.carouselItems;
		int i = 0;
		System.out.println(this.nbFinitions);
		for (MTRectangle carouselItem : carouselItems) {
			if (i < this.nbFinitions) {
				carouselItem.setWidthLocal(70);
				carouselItem.setHeightLocal(70);
				carouselItem.setNoStroke(true);
				carouselItem.setNoFill(true);
				carouselItem.setPickable(false);
				if (i < this.nbFinitions) {
					// rempli que la moiti� du caroussel
					addFinition(carouselItem, this.finitions.get(i), i);
				}

			}
			i++;
		}
		/***** Finitions carousel properties - end *****/
		/***** Finitions menu properties - begin *****/
		float roundFinitionMenuWidth = 700;
		float roundFinitionMenuHeight = 700;
		float roundFinitionMenuX = this.mtapp.getWidth()
				- (roundFinitionMenuWidth / 2) - 20;
		float roundFinitionMenuY = this.mtapp.getHeight()
				- (roundFinitionMenuHeight / 2);
		roundFinitionMenu = new MTRectangle(roundFinitionMenuX,
				roundFinitionMenuY, roundFinitionMenuWidth,
				roundFinitionMenuHeight, this.mtapp);
		PImage roundFinitionMenuImg = this.mtapp
				.loadImage("ressourcesGraphiquesNUI\\finitionsMenu.png");
		roundFinitionMenu.setTexture(roundFinitionMenuImg);
		roundFinitionMenu.setNoStroke(true);
		roundFinitionMenu.setNoFill(true);
		this.getCanvas().addChild(roundFinitionMenu);

		final Vector3D coords = roundFinitionMenu
				.getCenterPointRelativeToParent();
		final FixeElement fixeIt = new FixeElement(roundFinitionMenu, coords);
		roundFinitionMenu.addInputListener(fixeIt);
		TapAndHoldProcessor tapAndHoldsm = new TapAndHoldProcessor(mtapp);
		tapAndHoldsm.setHoldTime(bridge.getFastTapTime());
		roundFinitionMenu.registerInputProcessor(tapAndHoldsm);
		roundFinitionMenu.addGestureListener(TapAndHoldProcessor.class,
				new TapAndHoldVisualizer(mtapp, getCanvas()));
		roundFinitionMenu.addGestureListener(TapAndHoldProcessor.class,
				new IGestureEventListener() {
					public boolean processGestureEvent(MTGestureEvent ge) {
						TapAndHoldEvent t = (TapAndHoldEvent) ge;
						if (t.isHoldComplete()) {
							roundFinitionMenu.rotateZ(coords, 180);
						}
						return false;
					}
				});
		/***** Finitions menu properties - end *****/
		/***** Layer effect properties - end *****/
		/************* Initialize cursor click fonction - begin ************/
		int port = 3333;
		tuioMouse = new TuioMouse();
		TuioClient client = new TuioClient(port);
		client.addTuioListener(tuioMouse);
		/************* Initialize cursor click fonction - end ************/
	}

	/**
	 * Initialise le cadre � partir des dimensions de l'oeuvre d'art
	 * s�lectionn�e.
	 * 
	 * @param verticalSizeArtPiece
	 *            Dimension verticale de l'oeuvre
	 * @param horizontalSizeArtPiece
	 *            Dimension horizontale de l'oeuvre
	 */
	private void setCadreProperties(double verticalSizeArtPiece,
			double horizontalSizeArtPiece) {
		// Taile de l'oeuvre
		this.verticalSize = verticalSizeArtPiece;
		this.horizontalSize = horizontalSizeArtPiece;
		this.monCadre.setLongueurOeuvre(this.verticalSize);
		this.monCadre.setLargeurOeuvre(this.horizontalSize);
		this.monCadre.resetCadre();
		monCadre.setTailleRef(7, 40);
		this.moduleTailleBase = this.mtb * monCadre.getWidthRef();
		this.angleTailleBase = this.atb * monCadre.getWidthRef();
		this.profileWidth = this.pw * monCadre.getWidthRef();

		this.monCadre.setIdAndCadre();
		this.monCadre.setCadreName("CAD" + this.horizontalSize
				+ this.verticalSize);

		this.departCadreX = (int) (((actualPanelWidth - this.dragAndDropPanel_width) / 2) - ((this.monCadre
				.getTailleBrancheHaut() + this.angleTailleBase * 2) / 2));
		this.departCadreY = (int) (actualPanelHeight * 0.1);

		createElementsToDrag();

		this.topSideSize = this.monCadre.getTailleBrancheHaut();
		this.rightSideSize = this.monCadre.getTailleBrancheDroite();
		this.bottomSideSize = this.monCadre.getTailleBrancheBas();
		this.leftSideSize = this.monCadre.getTailleBrancheGauche();
		// Get finitions
		this.finitions = GestionnaireDonnees.getAllFinitions();
		this.nbFinitions = this.finitions.size();
	}

	/**
	 * Ajoute la finition au carousel de finitions de la sc�ne.
	 * 
	 * @param item
	 *            El�ment graphique sur lequel repose la finition
	 * @param finition
	 *            La finition
	 * @param i
	 *            Son emplacement dans le carousel
	 */
	public void addFinition(MTRectangle item, Finition finition, int i) {
		PImage finitionSupport = mtapp
				.loadImage("ressourcesGraphiquesNUI\\finitionSupport.png");
		item.setTexture(finitionSupport);
		MTEllipse finitionView = new MTEllipse(this.mtapp,
				item.getCenterPointLocal(), 50, 50);
		Color color = finition.getColor();
		MTColor finitionColor = null;
		if (finition.getColor() == null) {
			finitionColor = new MTColor(0, 0, 0, 0);
		} else {
			finitionColor = new MTColor(color.getRed(), color.getGreen(),
					color.getBlue());
		}
		;
		finitionView.setFillColor(finitionColor);
		finitionView.setPickable(false);
		item.setPickable(false);
		item.addChild(finitionView);
	}

	/**
	 * Affiche le cadre par d�faut, d'apr�s les dimensions de l'oeuvre
	 */
	private void displayDefaultCadre() {
		int departBrancheHautX = (int) (this.departCadreX + this.angleTailleBase);
		int departBrancheHautY = this.departCadreY;
		int departBrancheDroiteX = (int) (this.departCadreX
				+ this.angleTailleBase + this.topSideSize
				+ this.angleTailleBase - this.profileWidth);
		int departBrancheDroiteY = (int) (departCadreY + this.angleTailleBase);
		int departBrancheBasX = (int) (this.departCadreX + this.angleTailleBase);
		int departBrancheBasY = (int) (this.departCadreY + this.angleTailleBase
				+ this.angleTailleBase - this.profileWidth + this.rightSideSize);
		int departBrancheGaucheX = departCadreX;
		int departBrancheGaucheY = departBrancheDroiteY;
		// Decks
		int departAngleHautGaucheX = departCadreX;
		int departAngleHautGaucheY = departCadreY;
		int departAngleHautDroiteX = (int) (departBrancheDroiteX + this.profileWidth);
		int departAngleHautDroiteY = (int) (this.departCadreY);
		int departAngleBasDroiteX = departAngleHautDroiteX;
		int departAngleBasDroiteY = (int) (departBrancheBasY + this.profileWidth);
		int departAngleBasGaucheX = departCadreX;
		int departAngleBasGaucheY = (int) (departBrancheBasY + this.profileWidth);

		/************* Draw Cadre *************/
		/***** Cadre's Sides *****/
		// Draw Cadre's top side modules
		for (int i = 0; i < this.monCadre.getPositionHaut().size(); i++) {
			Module module = this.monCadre.getPositionHaut().get(i);
			module.setCoordX(departBrancheHautX);
			module.setCoordY(departBrancheHautY);

			Vertex[] vt = new Vertex[] {
					new Vertex(departBrancheHautX, departBrancheHautY),
					new Vertex(departBrancheHautX
							+ (float) this.moduleTailleBase, departBrancheHautY),
					new Vertex(departBrancheHautX
							+ (float) this.moduleTailleBase, departBrancheHautY
							+ this.profileWidth),
					new Vertex(departBrancheHautX, departBrancheHautY
							+ this.profileWidth) };
			MyDragAndDropTarget tsm = new MyDragAndDropTarget(vt, this.mtapp,
					font);
			this.getCanvas().getChildByName("Cadre Panel").addChild(tsm);
			tsm.addGestureListener(DragProcessor.class, new DragAndDropAction());
			tsm.setStrokeColor(new MTColor(0, 0, 0, 0));
			tsm.setFillColor(MTColor.WHITE);
			tsm.setName("1" + Integer.toString(i));
			tsm.setCSSID("topPosition");

			departBrancheHautX = departBrancheHautX + module.getWidth();
		}

		// Draw Cadre's right side modules
		for (int i = 0; i < this.monCadre.getPositionDroite().size(); i++) {
			// monCadre.getPositionDroite().get(i).setDrawingProportions(proportion);
			Module module = this.monCadre.getPositionDroite().get(i);
			module.setCoordX(departBrancheDroiteX);
			module.setCoordY(departBrancheDroiteY);

			Vertex[] vr = new Vertex[] {
					new Vertex(departBrancheDroiteX, departBrancheDroiteY),
					new Vertex(departBrancheDroiteX + this.profileWidth,
							departBrancheDroiteY),
					new Vertex(departBrancheDroiteX + this.profileWidth,
							departBrancheDroiteY
									+ (float) this.moduleTailleBase),
					new Vertex(departBrancheDroiteX, departBrancheDroiteY
							+ (float) this.moduleTailleBase) };
			MyDragAndDropTarget rsm = new MyDragAndDropTarget(vr, this.mtapp,
					font);
			this.getCanvas().getChildByName("Cadre Panel").addChild(rsm);
			rsm.addGestureListener(DragProcessor.class, new DragAndDropAction());
			rsm.setStrokeColor(new MTColor(0, 0, 0, 0));
			rsm.setFillColor(MTColor.WHITE);
			rsm.setName("2" + Integer.toString(i));
			rsm.setCSSID("rightPosition");

			departBrancheDroiteY = departBrancheDroiteY + module.getHeight();
		}
		// Draw Cadre's bottom side modules
		for (int i = 0; i < monCadre.getPositionBas().size(); i++) {
			Module module = monCadre.getPositionBas().get(i);
			module.setCoordX(departBrancheBasX);
			module.setCoordY(departBrancheBasY);

			Vertex[] vb = new Vertex[] {
					new Vertex(departBrancheBasX, departBrancheBasY),
					new Vertex(departBrancheBasX
							+ (float) this.moduleTailleBase, departBrancheBasY),
					new Vertex(departBrancheBasX
							+ (float) this.moduleTailleBase, departBrancheBasY
							+ this.profileWidth),
					new Vertex(departBrancheBasX, departBrancheBasY
							+ this.profileWidth) };
			MyDragAndDropTarget btm = new MyDragAndDropTarget(vb, this.mtapp,
					font);
			this.getCanvas().getChildByName("Cadre Panel").addChild(btm);
			btm.addGestureListener(DragProcessor.class, new DragAndDropAction());
			btm.setStrokeColor(new MTColor(0, 0, 0, 0));
			btm.setFillColor(new MTColor(MTColor.WHITE));
			btm.setName("3" + Integer.toString(i));
			btm.setCSSID("bottomPosition");

			departBrancheBasX = departBrancheBasX + module.getWidth();
		}
		// Draw Cadre's left side
		for (int i = 0; i < monCadre.getPositionGauche().size(); i++) {
			Module module = monCadre.getPositionGauche().get(i);
			module.setCoordX(departBrancheGaucheX);
			module.setCoordY(departBrancheGaucheY);

			Vertex[] vl = new Vertex[] {
					new Vertex(departBrancheGaucheX, departBrancheGaucheY),
					new Vertex(departBrancheGaucheX + this.profileWidth,
							departBrancheGaucheY),
					new Vertex(departBrancheGaucheX + this.profileWidth,
							departBrancheGaucheY
									+ (float) this.moduleTailleBase),
					new Vertex(departBrancheGaucheX, departBrancheGaucheY
							+ (float) this.moduleTailleBase) };
			MyDragAndDropTarget lsm = new MyDragAndDropTarget(vl, this.mtapp,
					font);
			this.getCanvas().getChildByName("Cadre Panel").addChild(lsm);
			lsm.addGestureListener(DragProcessor.class, new DragAndDropAction());
			lsm.setStrokeColor(new MTColor(0, 0, 0, 0));
			lsm.setFillColor(new MTColor(MTColor.WHITE));
			lsm.setName("4" + Integer.toString(i));
			lsm.setCSSID("leftPosition");

			departBrancheGaucheY = departBrancheGaucheY + module.getHeight();
		}
		/***** Cadre's Decks *****/
		// Draw Cadre's top-left deck square
		Vertex[] vtl = new Vertex[] {
				new Vertex(departAngleHautGaucheX, departAngleHautGaucheY),
				new Vertex(
						(float) (departAngleHautGaucheX + this.angleTailleBase),
						departAngleHautGaucheY),
				new Vertex(
						(float) (departAngleHautGaucheX + this.angleTailleBase),
						departAngleHautGaucheY + profileWidth),
				new Vertex(departAngleHautGaucheX + profileWidth,
						departAngleHautGaucheY + profileWidth),
				new Vertex(departAngleHautGaucheX + profileWidth,
						(float) (departAngleHautGaucheY + this.angleTailleBase)),
				new Vertex(departAngleHautGaucheX,
						(float) (departAngleHautGaucheY + this.angleTailleBase)) };
		MyDragAndDropTarget topLeftDeck = new MyDragAndDropTarget(vtl,
				this.mtapp, font);
		this.getCanvas().getChildByName("Cadre Panel").addChild(topLeftDeck);
		topLeftDeck.addGestureListener(DragProcessor.class,
				new DragAndDropAction());
		topLeftDeck.setStrokeColor(new MTColor(0, 0, 0, 0));
		topLeftDeck.setFillColor(new MTColor(MTColor.WHITE));
		topLeftDeck.setName("95");
		// Draw Cadre's top-right deck square
		Vertex[] vtr = new Vertex[] {
				new Vertex(departAngleHautDroiteX, departAngleHautDroiteY),
				new Vertex(
						(float) (departAngleHautDroiteX + this.angleTailleBase),
						departAngleHautDroiteY),
				new Vertex(
						(float) (departAngleHautDroiteX + this.angleTailleBase),
						departAngleHautDroiteY + profileWidth),
				new Vertex(departAngleHautDroiteX + profileWidth,
						departAngleHautDroiteY + profileWidth),
				new Vertex(departAngleHautDroiteX + profileWidth,
						(float) (departAngleHautDroiteY + this.angleTailleBase)),
				new Vertex(departAngleHautDroiteX,
						(float) (departAngleHautDroiteY + this.angleTailleBase)) };
		MyDragAndDropTarget topRightDeck = new MyDragAndDropTarget(vtr,
				this.mtapp, font);
		this.getCanvas().getChildByName("Cadre Panel").addChild(topRightDeck);
		topRightDeck.addGestureListener(DragProcessor.class,
				new DragAndDropAction());
		topRightDeck.setStrokeColor(new MTColor(0, 0, 0, 0));
		topRightDeck.setFillColor(new MTColor(MTColor.WHITE));
		topRightDeck.setName("96");
		topRightDeck.rotateZ(new Vertex(departAngleHautDroiteX,
				departAngleHautDroiteY), 90);
		// Draw Cadre's bottom-right deck square
		Vertex[] vbr = new Vertex[] {
				new Vertex(departAngleBasDroiteX, departAngleBasDroiteY),
				new Vertex(
						(float) (departAngleBasDroiteX + this.angleTailleBase),
						departAngleBasDroiteY),
				new Vertex(
						(float) (departAngleBasDroiteX + this.angleTailleBase),
						departAngleBasDroiteY + profileWidth),
				new Vertex(departAngleBasDroiteX + profileWidth,
						departAngleBasDroiteY + profileWidth),
				new Vertex(departAngleBasDroiteX + profileWidth,
						(float) (departAngleBasDroiteY + this.angleTailleBase)),
				new Vertex(departAngleBasDroiteX,
						(float) (departAngleBasDroiteY + this.angleTailleBase)) };
		MyDragAndDropTarget bottomRightDeck = new MyDragAndDropTarget(vbr,
				this.mtapp, font);
		this.getCanvas().getChildByName("Cadre Panel")
				.addChild(bottomRightDeck);
		bottomRightDeck.addGestureListener(DragProcessor.class,
				new DragAndDropAction());
		bottomRightDeck.setStrokeColor(new MTColor(0, 0, 0, 0));
		bottomRightDeck.setFillColor(new MTColor(MTColor.WHITE));
		bottomRightDeck.setName("97");
		bottomRightDeck.rotateZ(new Vertex(departAngleBasDroiteX,
				departAngleBasDroiteY), 180);
		// Draw Cadre's bottom-left deck square
		Vertex[] vbl = new Vertex[] {
				new Vertex(departAngleBasGaucheX, departAngleBasGaucheY),
				new Vertex(
						(float) (departAngleBasGaucheX + this.angleTailleBase),
						departAngleBasGaucheY),
				new Vertex(
						(float) (departAngleBasGaucheX + this.angleTailleBase),
						departAngleBasGaucheY + profileWidth),
				new Vertex(departAngleBasGaucheX + profileWidth,
						departAngleBasGaucheY + profileWidth),
				new Vertex(departAngleBasGaucheX + profileWidth,
						(float) (departAngleBasGaucheY + this.angleTailleBase)),
				new Vertex(departAngleBasGaucheX,
						(float) (departAngleBasGaucheY + this.angleTailleBase)) };
		MyDragAndDropTarget bottomLeftDeck = new MyDragAndDropTarget(vbl,
				this.mtapp, font);
		this.getCanvas().getChildByName("Cadre Panel").addChild(bottomLeftDeck);
		bottomLeftDeck.addGestureListener(DragProcessor.class,
				new DragAndDropAction());
		bottomLeftDeck.setStrokeColor(new MTColor(0, 0, 0, 0));
		bottomLeftDeck.setFillColor(new MTColor(MTColor.WHITE));
		bottomLeftDeck.setName("98");
		bottomLeftDeck.rotateZ(new Vertex(departAngleBasGaucheX,
				departAngleBasGaucheY), -90);
	}

	/**
	 * Cr�e les �l�ments de personnalisation, sous forme de variables
	 */
	private void createElementsToDrag() {
		this.modulesType = new ArrayList();
		// Module de 1 cm
		this.smallModule = new Module();
		smallModule.setTaille(1);
		smallModule.setHeightRef(this.monCadre.getHeightRef());
		smallModule.setWidthRef(this.monCadre.getWidthRef());
		smallModule.setPosition(1);
		addTypeOfModules(smallModule);
		// Module de 5 cm
		this.middleModule = new Module();
		middleModule.setTaille((int) (5));
		middleModule.setHeightRef(this.monCadre.getHeightRef());
		middleModule.setWidthRef(this.monCadre.getWidthRef());
		middleModule.setPosition(1);
		addTypeOfModules(middleModule);
		// Module de 7 cm
		this.bigModule = new Module();
		bigModule.setTaille((int) (7));
		bigModule.setHeightRef(this.monCadre.getHeightRef());
		bigModule.setWidthRef(this.monCadre.getWidthRef());
		bigModule.setPosition(1);
		addTypeOfModules(bigModule);
		// Angle
		this.deck = new Angle();
		deck.setHeightRef(this.monCadre.getHeightRef());
		deck.setWidthRef(this.monCadre.getWidthRef());
		deck.setPosition(1);
	}

	/**
	 * Ajoute le module au tableau des modules
	 * 
	 * @param module
	 *            Le module
	 */
	private void addTypeOfModules(Module module) {
		this.modulesType.add(module);
	}

	/**
	 * Cr�e les �l�ments de personnalisation, sous forme graphique
	 */
	private void diplayCadreElementsToDrag() {
		this.dndComponents = new HashMap<String, MTColor>();
		// 1 cm module
		if ((this.modulesType.get(0)) != null) {
			float x = dragAndDropPanel_positionX + 20;
			float y = dragAndDropPanel_positionY + 150;
			float width = (float) this.moduleTailleBase;
			float height = this.profileWidth;

			Vertex[] sm = new Vertex[] { new Vertex(x, y),
					new Vertex(x + width, y),
					new Vertex(x + width, y + height),
					new Vertex(x, y + height) };
			final MTPolygon smallModule = new MTPolygon(sm, this.mtapp);
			smallModule.setStrokeColor(new MTColor(0, 0, 0, 0));
			smallModule.setFillColor(new MTColor(MTColor.FUCHSIA));
			smallModule.setName("smallModule");

			final Vector3D coords = smallModule
					.getCenterPointRelativeToParent();
			final FixeElement fixeIt = new FixeElement(smallModule, coords);
			smallModule.addInputListener(fixeIt);
			this.getCanvas().getChildByName("Modules menu")
					.addChild(smallModule);
			smallModule.addGestureListener(DragProcessor.class,
					new PersoDropDrop(smallModule));
			TapAndHoldProcessor tapAndHoldsm = new TapAndHoldProcessor(mtapp);
			tapAndHoldsm.setHoldTime(bridge.getFastTapTime());
			smallModule.registerInputProcessor(tapAndHoldsm);
			smallModule.addGestureListener(TapAndHoldProcessor.class,
					new TapAndHoldVisualizer(mtapp, getCanvas()));
			smallModule.addGestureListener(TapAndHoldProcessor.class,
					new IGestureEventListener() {
						public boolean processGestureEvent(MTGestureEvent ge) {
							TapAndHoldEvent t = (TapAndHoldEvent) ge;
							if (t.getId() == t.GESTURE_STARTED) {
								bridge.setHoldHasBeenComplete(false);
							}
							if (t.isHoldComplete()) {
								bridge.setHoldHasBeenComplete(true);
								smallModule.removeInputListener(fixeIt);
							}
							return false;
						}
					});

			this.dndComponents.put(smallModule.getName(),
					smallModule.getFillColor());
		} else {
			System.out.println("NO SMALL MODULE");
		}
		// 5 cm module
		if ((this.modulesType.get(1)) != null) {
			float x = dragAndDropPanel_positionX + 20;
			float y = dragAndDropPanel_positionY + 250;
			float width = (float) this.moduleTailleBase * 10;
			float height = this.profileWidth;

			Vertex[] mm = new Vertex[] { new Vertex(x, y),
					new Vertex(x + width, y),
					new Vertex(x + width, y + height),
					new Vertex(x, y + height) };
			final MTPolygon middleModule = new MTPolygon(mm, this.mtapp);
			middleModule.setStrokeColor(new MTColor(0, 0, 0, 0));
			middleModule.setFillColor(new MTColor(MTColor.FUCHSIA));
			middleModule.setName("middleModule");

			final Vector3D coords = middleModule
					.getCenterPointRelativeToParent();
			final FixeElement fixeIt = new FixeElement(middleModule, coords);
			middleModule.addInputListener(fixeIt);
			this.getCanvas().getChildByName("Modules menu")
					.addChild(middleModule);
			middleModule.addGestureListener(DragProcessor.class,
					new PersoDropDrop(middleModule));
			TapAndHoldProcessor tapAndHoldmm = new TapAndHoldProcessor(mtapp);
			tapAndHoldmm.setHoldTime(bridge.getFastTapTime());
			middleModule.registerInputProcessor(tapAndHoldmm);
			middleModule.addGestureListener(TapAndHoldProcessor.class,
					new TapAndHoldVisualizer(mtapp, getCanvas()));
			middleModule.addGestureListener(TapAndHoldProcessor.class,
					new IGestureEventListener() {
						public boolean processGestureEvent(MTGestureEvent ge) {
							TapAndHoldEvent t = (TapAndHoldEvent) ge;
							if (t.getId() == t.GESTURE_STARTED) {
								bridge.setHoldHasBeenComplete(false);
							}
							if (t.isHoldComplete()) {
								bridge.setHoldHasBeenComplete(true);
								middleModule.removeInputListener(fixeIt);
							}
							return false;
						}
					});

			this.dndComponents.put(middleModule.getName(),
					middleModule.getFillColor());
		} else {
			System.out.println("NO MIDDLE MODULE");
		}
		// 7 cm module
		if ((this.modulesType.get(2)) != null) {
			float x = dragAndDropPanel_positionX + 20;
			float y = dragAndDropPanel_positionY + 350;
			float width = (float) this.moduleTailleBase * 7;
			float height = this.profileWidth;

			Vertex[] bm = new Vertex[] { new Vertex(x, y),
					new Vertex(x + width, y),
					new Vertex(x + width, y + height),
					new Vertex(x, y + height) };
			final MTPolygon bigModule = new MTPolygon(bm, this.mtapp);
			bigModule.setStrokeColor(new MTColor(0, 0, 0, 0));
			bigModule.setFillColor(new MTColor(MTColor.FUCHSIA));
			bigModule.setName("bigModule");

			final Vector3D coords = bigModule.getCenterPointRelativeToParent();
			final FixeElement fixeIt = new FixeElement(bigModule, coords);
			bigModule.addInputListener(fixeIt);
			this.getCanvas().getChildByName("Modules menu").addChild(bigModule);
			bigModule.addGestureListener(DragProcessor.class,
					new PersoDropDrop(bigModule));
			TapAndHoldProcessor tapAndHoldbm = new TapAndHoldProcessor(mtapp);
			tapAndHoldbm.setHoldTime(bridge.getFastTapTime());
			bigModule.registerInputProcessor(tapAndHoldbm);
			bigModule.addGestureListener(TapAndHoldProcessor.class,
					new TapAndHoldVisualizer(mtapp, getCanvas()));
			bigModule.addGestureListener(TapAndHoldProcessor.class,
					new IGestureEventListener() {
						public boolean processGestureEvent(MTGestureEvent ge) {
							TapAndHoldEvent t = (TapAndHoldEvent) ge;
							if (t.getId() == t.GESTURE_STARTED) {
								bridge.setHoldHasBeenComplete(false);
							}
							if (t.isHoldComplete()) {
								bridge.setHoldHasBeenComplete(true);
								bigModule.removeInputListener(fixeIt);
							}
							return false;
						}
					});
			this.dndComponents.put(bigModule.getName(),
					bigModule.getFillColor());
		} else {
			System.out.println("NO BIG MODULE");
		}

		// Angle
		if (this.deck != null) {
			float x = dragAndDropPanel_positionX + 20;
			float y = dragAndDropPanel_positionY + 450;
			this.angleTailleBase = this.angleTailleBase;
			this.profileWidth = this.profileWidth;
			Vertex[] v = new Vertex[] {
					new Vertex(x, y),
					new Vertex((float) (x + this.angleTailleBase), y),
					new Vertex((float) (x + this.angleTailleBase), y
							+ profileWidth),
					new Vertex(x + profileWidth, y + profileWidth),
					new Vertex(x + profileWidth,
							(float) (y + this.angleTailleBase)),
					new Vertex(x, (float) (y + this.angleTailleBase)) };
			final MTPolygon angle = new MTPolygon(v, this.mtapp);
			angle.setStrokeColor(new MTColor(0, 0, 0, 0));
			angle.setFillColor(new MTColor(MTColor.FUCHSIA));
			angle.setName("deck");
			angle.rotateZ(new Vertex(x, (float) (y + this.angleTailleBase)), 45);

			final Vector3D coords = angle.getCenterPointRelativeToParent();
			final FixeElement fixeIt = new FixeElement(angle, coords);
			angle.addInputListener(fixeIt);
			this.getCanvas().getChildByName("Modules menu").addChild(angle);
			angle.addGestureListener(DragProcessor.class, new PersoDropDrop(
					angle));
			TapAndHoldProcessor tapAndHold = new TapAndHoldProcessor(mtapp);
			tapAndHold.setHoldTime(bridge.getFastTapTime());
			angle.registerInputProcessor(tapAndHold);
			angle.addGestureListener(TapAndHoldProcessor.class,
					new TapAndHoldVisualizer(mtapp, getCanvas()));
			angle.addGestureListener(TapAndHoldProcessor.class,
					new IGestureEventListener() {
						public boolean processGestureEvent(MTGestureEvent ge) {
							TapAndHoldEvent t = (TapAndHoldEvent) ge;
							if (t.getId() == t.GESTURE_STARTED) {
								bridge.setHoldHasBeenComplete(false);
							}
							if (t.isHoldComplete()) {
								bridge.setHoldHasBeenComplete(true);
								angle.removeInputListener(fixeIt);
							}
							return false;
						}
					});
			this.dndComponents.put(angle.getName(), angle.getFillColor());
		} else {
			System.out.println("NO ANGLE");
		}
	}

	/**
	 * Place le visuel de l'oeuvre d'art dans le cadre
	 * 
	 * @param MTRectangle
	 *            Le visuel de l'oeuvre d'art
	 */
	private void displayArtPiece(MTRectangle artPiece) {
		float totalMargin = (float) 0.8;
		float ratio = 0;
		if (this.horizontalSize >= this.verticalSize) {
			ratio = (float) (this.horizontalSize / this.verticalSize);
		} else if (this.horizontalSize < this.verticalSize) {
			ratio = (float) (this.verticalSize / this.horizontalSize);
		}
		// Pour centrer l'oeuvre au milieu du cadre
		float internDimensionY = (float) ((this.angleTailleBase
				/ draggableElementEnlargement - this.profileWidth
				/ draggableElementEnlargement) * 2)
				+ this.monCadre.getTailleBrancheDroite();
		float internDimensionX = (float) (((this.angleTailleBase - this.profileWidth) * 2) + this.monCadre
				.getTailleBrancheBas());
		float marginToCenterY = 0;
		float marginToCenterX = 0;

		float width = (float) (this.monCadre.getTailleBrancheHaut()
				+ ((this.angleTailleBase / draggableElementEnlargement - this.profileWidth
						/ draggableElementEnlargement) * 2) + totalMargin);
		float height = width * ratio;
		if (internDimensionY > height) {
			marginToCenterY = (internDimensionY - height) / 2;
		} else if (internDimensionX > width) {
			marginToCenterX = (marginToCenterX - width) / 2;
		}
		float x = (float) (this.departCadreX + this.profileWidth
				/ draggableElementEnlargement - totalMargin / 2)
				+ marginToCenterX;
		float y = (float) (this.departCadreY + this.profileWidth
				/ draggableElementEnlargement - totalMargin / 2)
				+ marginToCenterY;

		MTRectangle artPieceImg = new MTRectangle(x, y, width, height,
				this.mtapp);
		artPieceImg.setTexture(artPiece.getTexture());
		artPieceImg.setNoStroke(true);
		artPieceImg.setNoFill(true);
		artPieceImg.setPickable(false);
		this.getCanvas().getChildByName("Cadre Panel").addChild(artPieceImg);
	}

	/**
	 * Classe interne permettant de fixer la position d'un �l�ment sujet � des
	 * actions de gestuelles
	 * 
	 * @author Robin Jespierre
	 *
	 */
	public class FixeElement implements IMTInputEventListener {
		private MTPolygon element;
		private Vector3D coords;

		public FixeElement(MTPolygon elementToFixe, Vector3D coordonees) {
			element = elementToFixe;
			coords = coordonees;
		}

		@Override
		public boolean processInputEvent(MTInputEvent arg0) {
			element.setPositionRelativeToParent(coords);
			return false;
		}

	}

	/**
	 * Classe interne disposant de toute la logique permettant de changer la
	 * finition d'un module ou d'un angle par du DnD
	 * 
	 * @author Robin Jespierre
	 *
	 */
	public class PersoDropDrop extends AbstractDnDAction {
		private MTPolygon originalComponent;
		private MTPolygon actualComponent;
		private HashMap<String, MTColor> selectedComponentsFinition;
		private String dropTargetName;
		private int cmptModule = 0; // odd:horizontal - even:vertical
		private int cmptAngle = 0; // 0:beginning(45�) - 1:tl - 2:tr - 3:br -
									// 4:bl

		public PersoDropDrop(MTPolygon component) {
			MTPolygon original = component;
			this.actualComponent = original;
			createOriginalComponent(original);
			this.selectedComponentsFinition = new HashMap<String, MTColor>();
			this.dropTargetName = "";
		}

		private void createOriginalComponent(MTPolygon original) {
			this.originalComponent = new MTPolygon(
					original.getVerticesGlobal(), mtapp);
			this.originalComponent.setStrokeColor(original.getStrokeColor());
			this.originalComponent.setFillColor(original.getFillColor());
			this.originalComponent.setName(original.getName());
		}

		private MTPolygon rotateComponent(MTPolygon component, float degree) {
			MTPolygon rotatedComponent = component;
			float x = rotatedComponent.getCenterPointGlobal().x;
			float y = rotatedComponent.getCenterPointGlobal().y;
			rotatedComponent.rotateZ(new Vertex(x, y), degree);
			return rotatedComponent;
		}

		@Override
		public boolean gestureDetected(MTGestureEvent g) {
			originalComponent.setFillColor(actualComponent.getFillColor());
			return false;
		}

		@Override
		public boolean gestureUpdated(MTGestureEvent g) {
			if (bridge.firstTimeSteady() == true) {
				cursor.setCursor(1);
				updateGesture(g);
			}
			return false;
		}

		@Override
		public boolean gestureEnded(MTGestureEvent g) {
			this.actualComponent = (MTPolygon) g.getCurrentTarget();
			if (bridge.firstTimeSteady() == true) {
				cursor.setCursor(0);
				// Si le composant n'est par sur une target, alors revient � sa
				// place
				if (this.detectDropTarget(g, false) != null) {
					placeNewElement(g, this.actualComponent.getFillColor());
				}
				initializeDraggedElement();
			}
			return false;
		}

		private void updateGesture(MTGestureEvent g) {
			// this.actualComponent = (MTPolygon) g.getCurrentTarget();
			if (this.selectedComponentsFinition != null) {
				dePreSelectComponents(g);
			}
			if (this.detectDropTarget(g, false) != null) {
				// Cursor = Selection
				cursor.setCursor(1);
				// Le componsant d�plac� est sur une zone de d�pose
				DropTarget dt = this.detectDropTarget(g, false);
				MyDragAndDropTarget mddt = (MyDragAndDropTarget) dt;
				dropTargetName = mddt.getName();
				// Change component direction depending on the target zone shape
				// direction
				if (this.actualComponent.getName().contains("Module")) {
					// Le composant d�plac� est un module
					if (dropTargetName.startsWith("2")
							|| dropTargetName.startsWith("4")) {
						if (this.cmptModule == 0) {
							this.actualComponent = rotateComponent(
									this.actualComponent, 90);
							this.cmptModule = 1;
						}
					} else if (dropTargetName.startsWith("1")
							|| dropTargetName.startsWith("3")) {
						if (!(this.cmptModule == 0)) {
							this.actualComponent = rotateComponent(
									this.actualComponent, 90);
							this.cmptModule = 0;
						}
					}
				} else if (this.actualComponent.getName().contains("deck")) {
					// Place l'angle en position haut-gauche
					switch (cmptAngle) {
					case 0:
						this.actualComponent = rotateComponent(
								this.actualComponent, -45);
						this.cmptAngle = 1;
						break;
					case 2:
						this.actualComponent = rotateComponent(
								this.actualComponent, -90);
						this.cmptAngle = 1;
						break;
					case 3:
						this.actualComponent = rotateComponent(
								this.actualComponent, 180);
						this.cmptAngle = 1;
						break;
					case 4:
						this.actualComponent = rotateComponent(
								this.actualComponent, 90);
						this.cmptAngle = 1;
						break;
					}

					// Place l'angle en position correspondant � la position de
					// l'angle de la zone de d�pose
					if (dropTargetName.startsWith("95")) {

					} else if (dropTargetName.startsWith("96")) {
						this.actualComponent = rotateComponent(
								this.actualComponent, 90);
						this.cmptAngle = 2;
					} else if (dropTargetName.startsWith("97")) {
						this.actualComponent = rotateComponent(
								this.actualComponent, 180);
						this.cmptAngle = 3;
					} else if (dropTargetName.startsWith("98")) {
						this.actualComponent = rotateComponent(
								this.actualComponent, -90);
						this.cmptAngle = 4;
					} else {
						this.actualComponent = rotateComponent(
								this.actualComponent, 45);
						this.cmptAngle = 0;
					}
				}
				preSelectComponents(g, MTColor.BLUE);
			} else {
				// Cursor = Standard
				cursor.setCursor(0);
			}
		}

		private void initializeDraggedElement() {
			final Vector3D coords = this.originalComponent
					.getCenterPointRelativeToParent();
			final FixeElement fixeIt = new FixeElement(this.originalComponent,
					coords);
			this.originalComponent.addInputListener(fixeIt);
			getCanvas().getChildByName("Modules menu").removeChild(
					this.actualComponent);
			this.originalComponent.addGestureListener(DragProcessor.class,
					new PersoDropDrop(this.originalComponent));

			getCanvas().getChildByName("Modules menu").addChild(
					this.originalComponent);
			TapAndHoldProcessor tapAndHold = new TapAndHoldProcessor(mtapp);
			tapAndHold.setHoldTime(bridge.getFastTapTime());
			this.originalComponent.registerInputProcessor(tapAndHold);
			this.originalComponent.addGestureListener(
					TapAndHoldProcessor.class, new TapAndHoldVisualizer(mtapp,
							getCanvas()));
			this.originalComponent.addGestureListener(
					TapAndHoldProcessor.class, new IGestureEventListener() {
						public boolean processGestureEvent(MTGestureEvent ge) {
							TapAndHoldEvent t = (TapAndHoldEvent) ge;
							if (t.isHoldComplete()) {
								bridge.setHoldHasBeenComplete(true);
								originalComponent.removeInputListener(fixeIt);
							}
							return false;
						}
					});
			this.originalComponent.setFillColor(actualComponent.getFillColor());
		}

		private void preSelectComponents(MTGestureEvent g, MTColor color) {
			DropTarget dt = this.detectDropTarget(g, false);
			MyDragAndDropTarget mddt = (MyDragAndDropTarget) dt;
			dropTargetName = mddt.getName();

			int moduleSize = 0;
			boolean droppingDeck = false;
			boolean targettingDeck = true;
			int targetZone = 0;
			int modulesQuantity = 0;
			int componentId = Integer.parseInt(dropTargetName);

			// D�fini la taille du module
			switch (actualComponent.getName()) {
			case "smallModule":
				moduleSize = smallModule.getTaille();
				droppingDeck = false;
				break;
			case "middleModule":
				moduleSize = middleModule.getTaille();
				droppingDeck = false;
				break;
			case "bigModule":
				moduleSize = bigModule.getTaille();
				droppingDeck = false;
				break;
			case "deck":
				droppingDeck = true;
				break;
			}

			// D�fini le type de la zone de d�pose
			switch (mddt.getCSSID()) {
			case "topPosition":
				targetZone = 1;
				modulesQuantity = topSideSize / monCadre.getWidthRef();
				break;
			case "rightPosition":
				targetZone = 2;
				modulesQuantity = rightSideSize / monCadre.getWidthRef();
				break;
			case "bottomPosition":
				targetZone = 3;
				modulesQuantity = bottomSideSize / monCadre.getWidthRef();
				break;
			case "leftPosition":
				targetZone = 4;
				modulesQuantity = leftSideSize / monCadre.getWidthRef();
				break;
			default:
				modulesQuantity = 0;
			}

			// Fabrique le code module de quantit�, d'apr�s la partie du cadre
			// concern�e
			modulesQuantity = Integer.parseInt(Integer.toString(targetZone)
					+ Integer.toString(modulesQuantity));
			// Fabrique l'ID du composant avec la taille du module int�gr�e
			int componentIdWithModuleSize = getSumOfIds(componentId,
					moduleSize, targetZone);

			if (dropTargetName.startsWith("9")) {
				// La cible est un angle
				if (droppingDeck == true) {
					// La source est un angle
					storeSelectedComponents(mddt.getName(), mddt.getFillColor());
					mddt.setFillColor(color);
				}
			} else {
				// La cible est un module
				if (droppingDeck == false) {
					// La cible est un module
					if ((componentIdWithModuleSize) <= modulesQuantity) {
						// Il y a assez de place sur � cet emplacement pour
						// d�poser un module de cette taille
						storeSelectedComponents(mddt.getName(),
								mddt.getFillColor());
						mddt.setFillColor(color);
						for (int i = 1; i < moduleSize; i++) {
							String nextComponentName = Integer
									.toString(getSumOfIds(componentId, i,
											targetZone));
							MTPolygon nextComponent = (MTPolygon) getCanvas()
									.getChildByName("Cadre Panel")
									.getChildByName(nextComponentName);
							storeSelectedComponents(nextComponent.getName(),
									nextComponent.getFillColor());
							nextComponent.setFillColor(color);
						}
					}
				}
			}
		}

		private void dePreSelectComponents(MTGestureEvent g) {
			for (String value : this.selectedComponentsFinition.keySet()) {
				MTColor previousFinition = this.selectedComponentsFinition
						.get(value);
				MTPolygon component = (MTPolygon) getCanvas().getChildByName(
						"Cadre Panel").getChildByName(value);
				component.setFillColor(previousFinition);
			}
		}

		private void storeSelectedComponents(String name, MTColor finition) {
			this.selectedComponentsFinition.put(name, finition);
		}

		private Map<String, MTColor> getSelectedComponents() {
			return this.selectedComponentsFinition;
		}

		private void placeNewElement(MTGestureEvent g, MTColor color) {
			DropTarget dt = this.detectDropTarget(g, false);
			MyDragAndDropTarget mddt = (MyDragAndDropTarget) dt;
			dropTargetName = mddt.getName();

			int moduleSize = 0;
			boolean droppingDeck = false;
			boolean targettingDeck = true;
			int targetZone = 0;
			int modulesQuantity = 0;
			int componentId = Integer.parseInt(dropTargetName);

			// D�fini la taille du module
			switch (actualComponent.getName()) {
			case "smallModule":
				moduleSize = smallModule.getTaille();
				droppingDeck = false;
				break;
			case "middleModule":
				moduleSize = middleModule.getTaille();
				droppingDeck = false;
				break;
			case "bigModule":
				moduleSize = bigModule.getTaille();
				droppingDeck = false;
				break;
			case "deck":
				droppingDeck = true;
				break;
			}

			// D�fini le type de la zone de d�pose
			switch (mddt.getCSSID()) {
			case "topPosition":
				targetZone = 1;
				modulesQuantity = topSideSize / monCadre.getWidthRef();
				break;
			case "rightPosition":
				targetZone = 2;
				modulesQuantity = rightSideSize / monCadre.getWidthRef();
				break;
			case "bottomPosition":
				targetZone = 3;
				modulesQuantity = bottomSideSize / monCadre.getWidthRef();
				break;
			case "leftPosition":
				targetZone = 4;
				modulesQuantity = leftSideSize / monCadre.getWidthRef();
				break;
			default:
				modulesQuantity = 0;
			}

			// Fabrique le code module de quantit�, d'apr�s la partie du cadre
			// concern�e
			modulesQuantity = Integer.parseInt(Integer.toString(targetZone)
					+ Integer.toString(modulesQuantity));
			// Fabrique l'ID du composant avec la taille du module int�gr�e
			int componentIdWithModuleSize = getSumOfIds(componentId,
					moduleSize, targetZone);

			if (dropTargetName.startsWith("9")) {
				// La cible est un angle
				if (droppingDeck == true) {
					// La source est un angle
					mddt.setFillColor(color);
				}
			} else {
				// La cible est un module
				if (droppingDeck == false) {
					// La source est un module
					if ((componentIdWithModuleSize) <= modulesQuantity) {
						// Il y a assez de place � cet emplacement pour d�poser un module de cette taille
						mddt.setFillColor(color);
						for (int i = 1; i < moduleSize; i++) {
							String nextComponentName = Integer
									.toString(getSumOfIds(componentId, i,
											targetZone));
							MTPolygon nextComponent = (MTPolygon) getCanvas()
									.getChildByName("Cadre Panel")
									.getChildByName(nextComponentName);
							nextComponent.setFillColor(color);
						}
					}
				}
			}
		}

		private int getSumOfIds(int idWidthZone, int size, int zoneToReAdd) {
			// Fabrique l'ID du composant avec la taille du module int�gr�e
			String componenIdWidthCodeZone = Integer.toString(idWidthZone)
					.substring(1);
			int newComponentId = Integer.parseInt(Integer.toString(zoneToReAdd)
					+ Integer.toString(Integer
							.parseInt(componenIdWidthCodeZone) + size));
			return newComponentId;
		}
	}
	/**
	 * Renvoie le curseur de la sc�ne
	 * @return InputCursor
	 */
	public InputCursor getInputCursor() {
		return this.cursorL;
	}

	/**
	 * Renvoie l'objet TuioMouse
	 * @return TuioMouse
	 */
	public TuioMouse getTuioMouse() {
		return this.tuioMouse;
	}

	/**
	 * Renvoie l'objet TuioCursor
	 * @return TuioCursor
	 */
	public TuioCursor getTuioCursor() {
		return this.tuioCursor;
	}

	@Override
	public void init() {
	}
}