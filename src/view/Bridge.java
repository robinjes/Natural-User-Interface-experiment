package view;

import java.util.Hashtable;

import kinectControl.Hand;

import org.mt4j.MTApplication;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.sceneManagement.Iscene;

import tuioProtocol.TuioMouse;
import TUIO.TuioCursor;

import ch.comem.modele.Cadre;

/**
 * @title Interface NUI -TPB2014, Bridge
 * Cette classe permet la liaison entre les diff�rentes couches du projet.
 * 
 * @author Robin Jespierre
 *
 */
public class Bridge {
	/************* Application properties *************/
	private MTApplication mtapp;
	private HomeScene homeScene;
	private MainScene mainScene;
	private SelectionScene selectionScene;
	private int tapSlowTime = 0;
	private int tapFastTime = 0;
	/************* Cadre properties *************/
	private Cadre monCadre;
	/************* Tuio properties *************/
	private MTPolygon caughtComponent = null;
	private InputCursor cursorL = null;
	private TuioMouse tuioMouse = null;
	private TuioCursor tc = null;
	private Hashtable<Integer, Hand> ccl = null;

	private boolean firstTimeSteady = true;
	private boolean holdHasBeenComplete = false;

	public Bridge() {

	}

	/**
	 * Ajoute l'application principale MT4J
	 * 
	 * @param MTApplication
	 *            L'application MT4J
	 */
	public void setMainApplication(MTApplication mtapp) {
		this.mtapp = mtapp;
	}

	/**
	 * Renvoie l'application principale MT4J
	 * 
	 * @return MTApplication
	 */
	public MTApplication getMainApplication() {
		return this.mtapp;
	}

	/**
	 * Ajoute le cadre dans le projet
	 * 
	 * @param Cadre
	 *            Le cadre du projet
	 */
	public void setMonCadre(Cadre monCadre) {
		this.monCadre = monCadre;
	}

	/**
	 * Lance graphiquement l'application. Change � la sc�ne de s�lection de
	 * l'oeuvre
	 */
	public void startApplication() {
		this.mtapp.changeScene(this.mtapp.getScene("Selection"));
	}

	/**
	 * Revient graphiquement � l'�cran d'accueil
	 */
	public void goToHome() {
		this.mtapp.changeScene(this.mtapp.getScene("Home"));
	}

	/**
	 * Ajoute la sc�ne de s�lection de l'oeuvre
	 * 
	 * @param SelectionScene
	 *            La sc�ne de s�lection de l'oeuvre
	 */
	public void setSelectionScene(SelectionScene selectionScene) {
		this.selectionScene = selectionScene;
	}

	/**
	 * Renvoie la sc�ne de s�lection de l'oeuvre
	 * 
	 * @return SelectionScene La sc�ne de s�lection de l'oeuvre
	 */
	public SelectionScene getSelectionScene() {
		return this.selectionScene;
	}

	/**
	 * Ajoute la sc�ne d'accueil
	 * 
	 * @param SelectionScene
	 *            La sc�ne d'accueil
	 */
	public void setHomeScene(HomeScene homeScene) {
		this.homeScene = homeScene;
	}

	/**
	 * Renvoie la sc�ne d'accueil
	 * 
	 * @return SelectionScene La sc�ne d'accueil
	 */
	public HomeScene getHomeScene() {
		return this.homeScene;
	}

	/**
	 * Ajoute la sc�ne principale
	 * 
	 * @param SelectionScene
	 *            La sc�ne principale
	 */
	public void setMainScene(MainScene mainScene) {
		this.mainScene = mainScene;
	}

	/**
	 * Renvoie la sc�ne principale
	 * 
	 * @return SelectionScene La sc�ne principale
	 */
	public MainScene getMainScene() {
		return this.mainScene;
	}

	/**
	 * Renvoie la sc�ne courante (visible)
	 * 
	 * @return Iscene
	 */
	public Iscene getCurrentScene() {
		return this.mtapp.getCurrentScene();
	}

	/**
	 * Ajoute le temps "lent", pour le chargement d'une action
	 * 
	 * @param tapSlowTime
	 *            Le temps "lent", pour le chargement d'une action
	 */
	public void setSlowTapTime(int tapSlowTime) {
		this.tapSlowTime = tapSlowTime;
	}

	/**
	 * Renvoie le temps "lent", pour le chargement d'une action
	 * 
	 * @return Le temps "lent", pour le chargement d'une action
	 */
	public int getSlowTapTime() {
		return this.tapSlowTime;
	}

	/**
	 * Ajoute le temps "rapide", pour le chargement d'une action
	 * 
	 * @param tapSlowTime
	 *            Le temps "rapide", pour le chargement d'une action
	 */
	public void setFastTapTime(int tapFastTime) {
		this.tapFastTime = tapFastTime;
	}

	/**
	 * Renvoie le temps "rapide", pour le chargement d'une action
	 * 
	 * @return Le temps "rapide", pour le chargement d'une action
	 */
	public int getFastTapTime() {
		return this.tapFastTime;
	}

	/**
	 * D�fini si la main a �t� d�tect�e fixe pour la premi�re fois ou non
	 * 
	 * @param value
	 *            La main a �t� d�tect�e fixe pour la premi�re fois ou non
	 */
	public void setFirstTimeSteady(boolean value) {
		this.firstTimeSteady = value;
	}

	/**
	 * Renvoie si la main a �t� d�tect�e fixe pour la premi�re fois ou non
	 * 
	 * @return firstTimeSteady La main a �t� d�tect�e fixe pour la premi�re fois
	 *         ou non
	 */
	public boolean firstTimeSteady() {
		return this.firstTimeSteady;
	}

	/**
	 * D�fini si le chargement d'une action a �t� compl�t�
	 * 
	 * @param value
	 *            Le chargement d'une action a �t� compl�t�
	 */
	public void setHoldHasBeenComplete(boolean value) {
		holdHasBeenComplete = value;
	}

	/**
	 * Renvoie si le chargement d'une action a �t� compl�t�
	 * 
	 * @return holdHasBeenComplete Le chargement d'une action a �t� compl�t�
	 */
	public boolean holdHasBeenComplete() {
		return holdHasBeenComplete;
	}

	/**
	 * Met � jour la liste des curseurs (Hand) pr�sent dans le serveur Tuio
	 * 
	 * @param ccl
	 *            CurrentCursorList
	 */
	public void updateCurrentCursorList(Hashtable<Integer, Hand> ccl) {
		this.ccl = ccl;
	}

	/**
	 * Renvoie la liste des curseurs (Hand) pr�sent dans le serveur Tuio
	 * 
	 * @return Hashtable<Integer, Hand> CurrentCursorList
	 */
	public boolean getCurrentCursorListState() {
		boolean state = false; // Empty
		if (this.ccl.isEmpty()) {
			state = false;
		} else {
			state = true;
		}
		return state;
	}

	/**
	 * Action de clic de la souris, renvoyant � la classe "TuioMouse"
	 */
	public void cursorPress() {
		if (getCurrentScene() instanceof MainScene) {
			this.cursorL = ((MainScene) getCurrentScene()).getInputCursor();
			this.tuioMouse = ((MainScene) getCurrentScene()).getTuioMouse();
			this.tc = ((MainScene) getCurrentScene()).getTuioCursor();
		} else if (getCurrentScene() instanceof HomeScene) {

		} else if (getCurrentScene() instanceof SelectionScene) {
			this.cursorL = ((SelectionScene) getCurrentScene())
					.getInputCursor();
			this.tuioMouse = ((SelectionScene) getCurrentScene())
					.getTuioMouse();
			this.tc = ((SelectionScene) getCurrentScene()).getTuioCursor();
		}

		if (this.cursorL != null) {
			float x = this.cursorL.getCurrentEvtPosX();
			float y = this.cursorL.getCurrentEvtPosY();

			if (getCurrentScene() instanceof MainScene) {
				this.caughtComponent = (MTPolygon) getCurrentScene()
						.getCanvas()
						.getChildByName("Modules menu")
						.getChildByName(
								getCurrentScene().getCanvas()
										.getComponentAt(x, y).getName());
				if (this.caughtComponent != null) {
					this.tuioMouse.addTuioCursor(tc);
				}
			} else if (getCurrentScene() instanceof SelectionScene) {
				this.tuioMouse.addTuioCursor(tc);
			}
		}
	}

	/**
	 * Action de relachement de la souris, renvoyant � la classe "TuioMouse"
	 */
	public void cursorRelease() {
		if (this.tc != null) {
			this.tuioMouse.removeTuioCursor(tc);
		}
		this.tc = null;
		this.caughtComponent = null;
	}
}
