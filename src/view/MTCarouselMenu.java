package view;
import org.mt4j.MTApplication;
import org.mt4j.components.TransformSpace;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.input.gestureAction.TapAndHoldVisualizer;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.flickProcessor.FlickEvent;
import org.mt4j.input.inputProcessors.componentProcessors.flickProcessor.FlickProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapEvent;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.util.MTColor;
import org.mt4j.util.animation.Animation;
import org.mt4j.util.animation.AnimationEvent;
import org.mt4j.util.animation.IAnimationListener;
import org.mt4j.util.animation.MultiPurposeInterpolator;
import org.mt4j.util.math.Vector3D;

import processing.core.PGraphics;

public class MTCarouselMenu extends MTEllipse {
	private MTApplication mtapp;
	public MTRectangle[] carouselItems;
	private float constantRotation = 0f;


	public MTCarouselMenu(MTApplication mtapp, Vector3D centerPoint, int segments, int elipseWidth) {
		super(mtapp, centerPoint, 10, 10);
		this.mtapp = mtapp;
		this.carouselItems = new MTRectangle[segments];
		setNoFill(true);
//		setNoStroke(true);
		setStrokeColor(MTColor.BLUE);
		setPickable(false);

		float theta = 360 / segments;

		float current = 0;
		for (int i = 0; i < segments; i++) {
			int sector = (int) (current / 90);
			current += theta;
			float rot = 0;
			switch (sector) {
			case 0:
				rot = 90 - current;
				break;
			case 1:
				rot = -(current - 90);
				break;
			case 2:
				rot = 270 - current;
				break;
			case 3:
				rot = -current % 270;
				break;
			default:
				break;
			}

			float x = elipseWidth * (float) Math.cos(Math.toRadians(current));
			float z = elipseWidth * (float) Math.sin(Math.toRadians(current));
			carouselItems[i] = addNewListItem(x, 0, z, rot, Integer.toString(i));
		}
	}

	private MTRectangle addNewListItem(float x, float y, float z, float rot,String name) {
		final MTRectangle rect1 = new MTRectangle(mtapp, 100, 100);
		rect1.setName(name);
		rect1.setPositionGlobal(getCenterPointGlobal());
		rect1.unregisterAllInputProcessors();
		rect1.removeAllGestureEventListeners();
		rect1.registerInputProcessor(new FlickProcessor());
		addChild(rect1);
		rect1.translate(new Vector3D(x, y, z));
		rect1.rotateY(rect1.getCenterPointGlobal(), rot);
		return rect1;
	}

	public float getConstantRotation() {
		return constantRotation;
	}

	public void setConstantRotation(float constantRotation) {
		this.constantRotation = constantRotation;
	}

	@Override
	public void preDraw(PGraphics g) {
		super.preDraw(g);
		rotateY(getCenterPointGlobal(), constantRotation);
	}
}