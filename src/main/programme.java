package main;

import java.awt.Frame;

import kinectControl.GestureDetect;
import kinectControl.NIContext;
import kinectControl.NativeLoader;

import org.mt4j.MTApplication;
import org.mt4j.input.inputSources.AbstractInputSource;
import org.mt4j.input.inputSources.Tuio2dObjectInputSource;

import view.Bridge;
import view.HomeScene;
import view.MainScene;
import view.SelectionScene;
import ch.comem.ihm.Ihm;
import ch.comem.modele.Cadre;

/**
 * @title Interface NUI - TPB2014
 * Ce programme est le r�sultat de recherche sur les interfaces NUI. Il a �t� d�velopp� dans le cadre d'un projet de bachelor 2014 � la HEIG-VD d'Yverdon-les-bains, Suisse.
 * Il consiste � la mise en place d'un prototype d'interface NUI - Natural User Interface - contr�l�e par le capteur Kinect, pour une application existante, celle-ci permettant l'encadrement virtuel d'oeuvre d'arts.
 * Ce programme utilise plusieurs outils: OpenNi&Nite, le protocole TUIO et le Framework MT4J.
 * Il reprend certaines parties de projets d�j� existants, cit�s dans les classes concern�es.
 * @author Robin Jespierre
 *
 */
public class programme extends MTApplication {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Frame[] frames;
	private static Ihm ihm;
	private static Bridge bridge;
	private static Cadre monCadre;
	private static GestureDetect gd;
	private static int panelWidth;
	private static int panelHeight;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		NativeLoader.loadNatives();
		NIContext.init(args.length > 1 ? args[1] : null);
		ihm = new Ihm();
		bridge = new Bridge();
		monCadre = ihm.getMonCadre();
		frames = ihm.getFrames();
		//Ferme l'interface GUI d'origine pr�sente dans l'application existante
		frames[0].dispose();
		initialize();
		gd = new GestureDetect(3333,bridge,NIContext.get());
		gd.setPanelSize(panelWidth, panelHeight);
	}

	@Override
	public void startUp() {
		this.panelWidth = this.getWidth();
		this.panelHeight = this.getHeight();
		// Intialise les variables li�es � l'application
		bridge.setMainApplication(this);
		bridge.setMonCadre(monCadre);
		bridge.setSlowTapTime(1000);
		bridge.setFastTapTime(2000);

		//Initialise les composants graphiques MT4J
		AbstractInputSource[] allSources = this.getInputManager().getInputSources();
		Tuio2dObjectInputSource t2dois = new Tuio2dObjectInputSource(this);
		this.getInputManager().registerInputSource(t2dois);
		HomeScene homescene = new HomeScene(this, "Home", monCadre, bridge);
		bridge.setHomeScene(homescene);
		addScene(homescene);
		SelectionScene selectionScene = new SelectionScene(this, "Selection", this.monCadre, bridge);
		bridge.setSelectionScene(selectionScene);
		addScene(selectionScene);
		MainScene mainScene = new MainScene(this, "Main", this.monCadre, bridge);
		bridge.setMainScene(mainScene);
		addScene(mainScene);
		changeScene(selectionScene);
	}
}
