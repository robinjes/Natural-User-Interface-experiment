package tuioProtocol;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;

import TUIO.TuioClient;
import TUIO.TuioCursor;
import TUIO.TuioListener;
import TUIO.TuioObject;
import TUIO.TuioTime;

/**
 * Classe reprise du projet "TUIO Generator - part of the reacTIVision project",
 * pour le projet "Interface NUI -TPB2014". Cette classe permet la simulation de
 * la souris � partir des donn�es r�cup�r�es (client) sur le serveur TUIO.
 * 
 * @author Martin Kaltenbrunner, arrang� par Robin Jespierre
 *
 */
public class TuioMouse implements TuioListener {

	private Robot robot = null;
	private int width = 0;
	private int height = 0;
	private long mouse = -1; // Mouse is released (up)

	public void addTuioObject(TuioObject tobj) {
	}

	public void updateTuioObject(TuioObject tobj) {
	}

	public void removeTuioObject(TuioObject tobj) {
	}

	public void refresh(TuioTime bundleTime) {
	}

	public void addTuioCursor(TuioCursor tcur) {
		if (mouse < 0) {
			mouse = tcur.getSessionID();
			if (robot != null)
				robot.mouseMove((int) tcur.getX(), (int) tcur.getY());
			if (robot != null) {
				robot.mousePress(InputEvent.BUTTON1_MASK);
			}
		} else {
			if (robot != null)
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
			if (robot != null)
				robot.mousePress(InputEvent.BUTTON1_MASK);
		}
	}

	public void updateTuioCursor(TuioCursor tcur) {
		if (mouse == tcur.getSessionID()) {
			if (robot != null)
				robot.mouseMove((int) tcur.getX(), (int) tcur.getY());
		}
	}

	public void removeTuioCursor(TuioCursor tcur) {
		if (mouse == tcur.getSessionID()) {
			mouse = -1;
			if (robot != null)
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
		}

	}

	public TuioMouse() {
		try {
			robot = new Robot();
		} catch (Exception e) {
			System.out.println("failed to initialize mouse robot");
			System.exit(0);
		}

		width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	}

}