package kinectControl;

import org.OpenNI.*;
import com.primesense.NITE.*;

/**
 * Classe reprise de Andrew Davison, November 2011, ad@fivedots.psu.ac.th, pour
 * le projet "Interface NUI -TPB2014". Cette classe permet d'emmagasiner les
 * informations du point de la main obtenues par les points de contr�le Nite
 * 
 * @author Andrew Davison, arrang� par Robin Jespierre
 *
 */
public class PositionInfo {
	private int id; // ID de la main
	private Point3D pos; // Position 3D de la main dans le "real world"
	private float time;

	public PositionInfo(HandPointContext hpc) {
		id = hpc.getID();
		pos = hpc.getPosition();
		time = hpc.getTime();
	}

	public synchronized void update(HandPointContext hpc) {
		if (id == hpc.getID()) {
			pos = hpc.getPosition();
			time = hpc.getTime();
		}
	}

	public int getID() {
		return id;
	}

	public synchronized Point3D getPosition() {
		return pos;
	}

	public synchronized float getTime() {
		return time;
	}

	public synchronized String toString() {
		return String.format(
				"Hand Point %d at (%.0f, %.0f, %.0f) at %.0f secs", id,
				pos.getX(), pos.getY(), pos.getZ(), time);
	}

}
