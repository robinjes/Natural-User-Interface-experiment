package kinectControl;

import java.lang.reflect.Field;

/**
 * Permet de configurer le dossier source des librairies
 *
 */
public class NativeLoader {

	public static void loadNatives() {
		try {
			Class clazz = ClassLoader.class;
			Field field = clazz.getDeclaredField("sys_paths");
			boolean accessible = field.isAccessible();
			if (!accessible)
				field.setAccessible(true);
			Object original = field.get(clazz);

			field.set(clazz, null);

			try {
				System.setProperty("java.library.path", "lib/");
				System.loadLibrary("OpenNI.jni");
			} finally {
				field.set(clazz, original);
				field.setAccessible(accessible);
			}
		} catch (Exception e) {
			System.err.println("Unable to load require libraries:");
			e.printStackTrace();
		}
	}
}
