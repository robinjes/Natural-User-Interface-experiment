package kinectControl;

import org.OpenNI.GeneralException;
import org.OpenNI.OutArg;
import org.OpenNI.ScriptNode;
import org.OpenNI.Context;

/**
 * Classe reprise du projet "HandTuio", pour le projet "Interface NUI -TPB2014".
 * Cette classe permet de reprendre les information de configuration de OpenNi � partir d'un fichier XML externe � l'application
 * @author Michel Lawrie, arrang� par Robin Jespierre
 *
 */
public class NIContext {

	private static Context context;
	private static OutArg<ScriptNode> scriptNode;

	public static void init(String path) {
		scriptNode = new OutArg<ScriptNode>();
	    try {
			context = Context.createFromXmlFile(path == null ? "OpenNIConfig.xml" : path, scriptNode);
		} catch (GeneralException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static Context get() {
		return context;
	}

}