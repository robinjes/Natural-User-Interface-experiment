package kinectControl;

import org.OpenNI.*;
import org.mt4j.input.IMTInputEventListener;

import tuioProtocol.TUIOGenerator;
import view.Bridge;
import view.MainScene;
import TUIO.TuioClient;

import com.primesense.NITE.*;
import com.primesense.NITE.Direction;

/**
 * Classe reprise de Andrew Davison, November 2011, ad@fivedots.psu.ac.th, pour
 * le projet "Interface NUI -TPB2014". Cette classe permet de configurer les
 * outils pour la manipulation de la Kinect : OpenNi et Nite, pour le rep�rage
 * de mains Elle envoie les donn�es de position des mains sur le serveur TUIO
 * 
 * @author Andrew Davison, arrang� par Robin Jespierre
 *
 */
public class GestureDetect {
	/**
	 * 
	 */
	private static final long serialVersionUID = -763291769145763938L;
	public static String TUIO_HOST = "127.0.0.1";
	public static int TUIO_PORT = 3333;
	final TUIOGenerator tuioGenerator = new TUIOGenerator(TUIO_HOST, TUIO_PORT);

	private static int panelWidth;
	private static int panelHeight;

	private Bridge bridge;

	// Variables OpenNI and NITE
	private Context context;
	private SessionManager sessionMan;

	private boolean isRunning = true;
	private PositionInfo pi = null; // Permet d'emmagasiner les informations du
									// poin de la main

	public GestureDetect(int port, Bridge ai, Context context) {
		this.bridge = ai;
//		this.context = context;
		try {
			this.context = new Context();
		} catch (GeneralException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			configOpenNI();
			configNITE();

			System.out.println();
			System.out.println("Make a wave gesture to start the session");
			while (isRunning) {
				this.context.waitAnyUpdateAll();
				sessionMan.update(this.context);
			}
			context.release();
		} catch (GeneralException e) {
			System.out.println("The Kinect may not be wired");
			e.printStackTrace();
		}
	}

	/****************************************** Configuration de OpenNi et Nite ******************************************/
	/**
	 * Param�trage de la gestuelle et des Hands Generators dans OpenNi
	 */
	private void configOpenNI() {
		try {
			HandsGenerator handsGen = HandsGenerator.create(this.context);
			handsGen.SetSmoothing(0.1f); // 0-1: 0 signifie pas "smooth" et 1
											// signifie "infini"
			setHandEvents(handsGen);

			GestureGenerator gestureGen = GestureGenerator.create(this.context);
			setGestureEvents(gestureGen);

			this.context.startGeneratingAll();
			System.out.println("Le capteur Kinect fonctionne... ");
		} catch (GeneralException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Param�tre les d�tecteurs Nite
	 */
	private void configNITE() {
		try {
			// "Wave" permet de lancer la session lorsque l'utilisateur fait 2
			// aller-retours avec sa main
			sessionMan = new SessionManager(this.context, "Wave", "RaiseHand");
			setSessionEvents(sessionMan);

			// Cr�ation des d�tecteurs Nite: Steady et Swipe et les connecte au
			// gestionnaire de session
			PointControl pointCtrl = initPointControl();
			sessionMan.addListener(pointCtrl);

			SteadyDetector sdd = initSteadyDetector();
			sessionMan.addListener(sdd);

			SwipeDetector sd = initSwipeDetector();
			sessionMan.addListener(sd);
		} catch (GeneralException e) {
			e.printStackTrace();
			tuioGenerator.quit();
			System.exit(1);
		}
	}

	public void setPanelSize(int width, int height) {
		this.panelWidth = width;
		this.panelHeight = height;
	}

	/****************************************** Configuration des appels (callbacks) ******************************************/
	/**
	 * Cr�ation des callbacks d'�v�nements
	 * 
	 * @param handsGen
	 */
	private void setHandEvents(HandsGenerator handsGen)
	// create HandsGenerator callbacks
	{
		try {
			// Appel� si une main est cr��e
			handsGen.getHandCreateEvent().addObserver(
					new IObserver<ActiveHandEventArgs>() {
						public void update(
								IObservable<ActiveHandEventArgs> observable,
								ActiveHandEventArgs args) {
							int id = args.getId();
							Point3D pt = args.getPosition();
							float time = args.getTime();
							System.out
									.printf("Hand %d located at (%.0f, %.0f, %.0f), at %.0f secs\n",
											id, pt.getX(), pt.getY(),
											pt.getZ(), time);
						}
					});
			// Appel� si une main est d�truite
			handsGen.getHandDestroyEvent().addObserver(
					new IObserver<InactiveHandEventArgs>() {
						public void update(
								IObservable<InactiveHandEventArgs> observable,
								InactiveHandEventArgs args) {
							int id = args.getId();
							float time = args.getTime();
							System.out.printf(
									"Hand %d destroyed at %.0f secs \n", id,
									time);
						}
					});
		} catch (StatusException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cr�ation des callbacks de gestes
	 * 
	 * @param gestureGen
	 */
	private void setGestureEvents(GestureGenerator gestureGen) {
		try {
			// Appel� si un geste est reconnu
			gestureGen.getGestureRecognizedEvent().addObserver(
					new IObserver<GestureRecognizedEventArgs>() {
						public void update(
								IObservable<GestureRecognizedEventArgs> observable,
								GestureRecognizedEventArgs args) {
							String gestureName = args.getGesture();
							Point3D idPt = args.getIdPosition();
							// Position de la main lorsque le geste est reconnu
							Point3D endPt = args.getEndPosition();
							// Position de la main � la fin de la reconnaissance
							// du geste
						}
					});
		} catch (StatusException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cr�ation des callbacks de sessions
	 * 
	 * @param sessionMan
	 */
	private void setSessionEvents(SessionManager sessionMan) {
		try {
			// Appel� quand un geste de focus a commenc� � �tre reconnu
			sessionMan.getSessionFocusProgressEvent().addObserver(
					new IObserver<StringPointValueEventArgs>() {
						public void update(
								IObservable<StringPointValueEventArgs> observable,
								StringPointValueEventArgs args) {
							Point3D focusPt = args.getPoint();
							float progress = args.getValue();
							String focusName = args.getName();
							bridge.startApplication();
							bridge.cursorRelease();
							System.out
									.printf("Session focused at (%.0f, %.0f, %.0f) on %s [progress %.2f]\n",
											focusPt.getX(), focusPt.getY(),
											focusPt.getZ(), focusName, progress);
						}
					});

			// La session � commenc�e
			sessionMan.getSessionStartEvent().addObserver(
					new IObserver<PointEventArgs>() {
						public void update(
								IObservable<PointEventArgs> observable,
								PointEventArgs args) {
							Point3D focusPt = args.getPoint();
							System.out.printf(
									"Session started at (%.0f, %.0f, %.0f)\n",
									focusPt.getX(), focusPt.getY(),
									focusPt.getZ());
						}
					});

			// La session est termin�e
			sessionMan.getSessionEndEvent().addObserver(
					new IObserver<NullEventArgs>() {
						public void update(
								IObservable<NullEventArgs> observable,
								NullEventArgs args) {
							System.out.println("Session ended");
							bridge.goToHome(); // Revient � la fen�tre d'accueil
							isRunning = true; // L'application continue de
												// fonctionner

						}
					});
		} catch (StatusException e) {
			e.printStackTrace();
		}
	}

	/****************************************** Configuration des d�tecteurs et points de controle ******************************************/
	private PointControl initPointControl() {
		PointControl pointCtrl = null;
		try {
			pointCtrl = new PointControl();
			// Cr�ation d'un nouveau point de la main
			pointCtrl.getPointCreateEvent().addObserver(
					new IObserver<HandEventArgs>() {
						public void update(
								IObservable<HandEventArgs> observable,
								HandEventArgs args) {
							pi = new PositionInfo(args.getHand());

							// Envoie sur le serveur TUIO - La cr�ation commence
							HandPointContext handContext = args.getHand();
							int id = handContext.getID();
							Point3D pt = handContext.getPosition();
							Hand touch = new Hand(id, (int) pt.getX(), (int) pt
									.getY(), (int) pt.getZ(), panelWidth,
									panelHeight);
							tuioGenerator.currentCursorList.put(id, touch);
							tuioGenerator.cursorMessage(touch);
							System.out.println(tuioGenerator.currentCursorList);
							bridge.updateCurrentCursorList(tuioGenerator.currentCursorList);
							// Envoie sur le serveur TUIO - Cr�ation termin�e
						}
					});

			// Le point de la main s'est d�plac�
			pointCtrl.getPointUpdateEvent().addObserver(
					new IObserver<HandEventArgs>() {
						public void update(
								IObservable<HandEventArgs> observable,
								HandEventArgs args) {
							HandPointContext handContext = args.getHand();
							if (pi == null)
								pi = new PositionInfo(handContext);
							else
								pi.update(handContext);

							int id = handContext.getID();

							if (!tuioGenerator.currentCursorList
									.containsKey(id)) {

								// Envoie sur le serveur TUIO - La cr�ation
								// commence
								Point3D pt = handContext.getPosition();
								Hand touch = new Hand(id, (int) pt.getX(),
										(int) pt.getY(), (int) pt.getZ(),
										panelWidth, panelHeight);
								tuioGenerator.currentCursorList.put(id, touch);
								tuioGenerator.cursorMessage(touch);
								System.out
										.println(tuioGenerator.currentCursorList);
								bridge.updateCurrentCursorList(tuioGenerator.currentCursorList);
								// }
							} else {
								// Envoie sur le serveur TUIO - Le changement
								// commence
								Point3D pt = handContext.getPosition();
								if (pt == null)
									return;

								Hand touch = tuioGenerator.currentCursorList
										.get(id);
								touch.update((int) pt.getX(), (int) pt.getY(),
										(int) pt.getZ());
								tuioGenerator.cursorMessage(touch);
							}
						}
					});

			// D�truit le point de la main
			pointCtrl.getPointDestroyEvent().addObserver(
					new IObserver<IdEventArgs>() {
						public void update(IObservable<IdEventArgs> observable,
								IdEventArgs args) {
							int id = args.getId();
							System.out.printf("Point %d destroyed:\n", id);
							if (pi.getID() == id) {
								pi = null;
							}

							if (tuioGenerator.currentCursorList.containsKey(id)) {
								// Envoie sur le serveur TUIO - La destruction
								// commence
								tuioGenerator.currentCursorList.remove(id);
								tuioGenerator.cursorDelete();
								bridge.updateCurrentCursorList(tuioGenerator.currentCursorList);
								// Envoie sur le serveur TUIO - Destruction
								// termin�e
							}
						}
					});

		} catch (GeneralException e) {
			e.printStackTrace();
		}
		return pointCtrl;
	}

	/**
	 * D�tecteur de point (main) fixe
	 * 
	 * @return SteadyDetector
	 */
	private SteadyDetector initSteadyDetector() {
		try {
			final SteadyDetector steadyDetector = new SteadyDetector();
			// Configuration du temps pendant lequel la main doit rester fixe
			// pour �tre rep�r�e comme tel
			steadyDetector.setDetectionDuration(1000);
			steadyDetector.getSteadyEvent().addObserver(
					new IObserver<IdValueEventArgs>() {
						public void update(
								IObservable<IdValueEventArgs> observable,
								IdValueEventArgs args) {
							// Action si la main est d�tect�e comme fixe
							if (bridge.getCurrentScene() instanceof MainScene) {
								// Algorithme D&N permettant de simuler, sur un
								// lot de 2 "steady", un mouse down si la main
								// est fixe pour la premi�re fois, et siuler un
								// mouse up si la main est fixe pour la deuxi�me
								// fois
								if (bridge.firstTimeSteady() == true) {
									bridge.cursorPress();
									bridge.setFirstTimeSteady(false);
									System.out.println("First time steady!");
								} else if (bridge.firstTimeSteady() == false) {
									if (bridge.holdHasBeenComplete() == true) {
										bridge.cursorRelease();
										bridge.setFirstTimeSteady(true);
										bridge.setHoldHasBeenComplete(false);
										System.out
												.println("Steady again! Got a component!");
									} else if (bridge.holdHasBeenComplete() == false) {
										bridge.cursorPress();
										System.out
												.println("First time steady! Didn't got any component");
										bridge.setFirstTimeSteady(false);
									}
								}
							} else {
								bridge.cursorPress();
							}
						}
					});
			return steadyDetector;
		} catch (GeneralException e) {
			e.printStackTrace();
		}
		return null;
	}

	private SwipeDetector initSwipeDetector() {
		SwipeDetector swipeDetector = null;
		try {
			swipeDetector = new SwipeDetector();

			// Rep�re le swipe vers la gauche
			swipeDetector.getSwipeLeftEvent().addObserver(
					new IObserver<VelocityAngleEventArgs>() {
						public void update(
								IObservable<VelocityAngleEventArgs> observable,
								VelocityAngleEventArgs args) {
							bridge.getSelectionScene().carouselTurnLeft();
							System.out.printf("LEFT SWIPE DETECTED");
						}
					});
			// Rep�re le swipe vers le bas
			swipeDetector.getSwipeDownEvent().addObserver(
					new IObserver<VelocityAngleEventArgs>() {
						public void update(
								IObservable<VelocityAngleEventArgs> observable,
								VelocityAngleEventArgs args) {
							bridge.getSelectionScene().carouselTurnLeft();
							System.out.printf("DOWN SWIPE DETECTED");
						}
					});
			// Rep�re le swipe vers la droite
			swipeDetector.getSwipeRightEvent().addObserver(
					new IObserver<VelocityAngleEventArgs>() {
						public void update(
								IObservable<VelocityAngleEventArgs> observable,
								VelocityAngleEventArgs args) {
							bridge.getSelectionScene().carouselTurnRight();
							System.out.printf("RIGHT SWIPE DETECTED");
						}
					});
			// Rep�re le swipe vers le haut
			swipeDetector.getSwipeUpEvent().addObserver(
					new IObserver<VelocityAngleEventArgs>() {
						public void update(
								IObservable<VelocityAngleEventArgs> observable,
								VelocityAngleEventArgs args) {
							bridge.getSelectionScene().carouselTurnRight();
							System.out.printf("UP SWIPE DETECTED");
						}
					});
		} catch (GeneralException e) {
			e.printStackTrace();
		}
		return swipeDetector;
	}
}
