package kinectControl;

import java.awt.*;
import java.util.*;

/**
 * Classe reprise du projet "TUIO Simulator - part of the reacTIVision project",
 * pour le projet "Interface NUI -TPB2014". Cette classe r�cup�re les
 * informations du point de la main.
 * 
 * @author Martin Kaltenbrunner, arrang� par Robin Jespierre
 *
 */
public class Hand {

	public int session_id;

	public float xspeed, yspeed, mspeed, maccel;
	private long lastTime;
	public Vector<Point> path;
	private int zLoc;
	private float panelWidth;
	private float panelHeight;

	public Hand(int s_id, int xpos, int ypos, int zLoc, float panelWidth,
			float panelHeight) {

		this.session_id = s_id;
		this.zLoc = zLoc;

		path = new Vector<Point>();
		path.addElement(new Point(xpos, ypos));
		this.xspeed = 0.0f;
		this.yspeed = 0.0f;
		this.maccel = 0.0f;
		this.panelWidth = panelWidth;
		this.panelHeight = panelHeight;

		lastTime = System.currentTimeMillis();
	}

	public final void update(int xpos, int ypos, int zLoc) {

		Point lastPoint = getPosition();
		path.addElement(new Point(xpos, ypos));

		this.zLoc = zLoc;

		long currentTime = System.currentTimeMillis();
		float dt = (currentTime - lastTime) / 1000.0f;

		if (dt > 0) {
			float dx = (xpos - lastPoint.x) / this.panelWidth;
			float dy = (ypos - lastPoint.y) / this.panelHeight;
			float dist = (float) Math.sqrt(dx * dx + dy * dy);
			float new_speed = dist / dt;
			this.xspeed = dx / dt;
			this.yspeed = dy / dt;
			this.maccel = (new_speed - mspeed) / dt;
			this.mspeed = new_speed;
		}
		lastTime = currentTime;
	}

	public final void stop() {
		lastTime = System.currentTimeMillis();
		this.xspeed = 0.0f;
		this.yspeed = 0.0f;
		this.maccel = 0.0f;
		this.mspeed = 0.0f;
	}

	public final Point getPosition() {
		return path.lastElement();
	}

	public final Vector<Point> getPath() {
		return path;
	}

	public int getZLoc() {
		return zLoc;
	}

}