package cursor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.mt4j.AbstractMTApplication;
import org.mt4j.MTApplication;
import org.mt4j.components.MTComponent;
import org.mt4j.components.bounds.IBoundingShape;
import org.mt4j.components.visibleComponents.shapes.AbstractShape;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.components.visibleComponents.widgets.MTOverlayContainer;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputData.MTInputEvent;
import org.mt4j.input.inputProcessors.globalProcessors.AbstractGlobalInputProcessor;
import org.mt4j.input.inputProcessors.globalProcessors.CursorTracer;
import org.mt4j.sceneManagement.Iscene;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import processing.core.PApplet;
import processing.core.PImage;

/**
 * Classe CursorTracer. Un input processor d'entr�e qui traque tous les
 * �v�nements de type AbstractCursorInputEvt et affiche un cercle � leur
 * position
 * 
 * @author Christopher Ruff, arrang� par Robin Jespierre
 */
public class MyCursor extends AbstractGlobalInputProcessor {

	private MTApplication app;

	private Map<InputCursor, AbstractShape> cursorIDToDisplayShape;

	private Iscene scene;

	private MTComponent overlayGroup;

	private String path;

	private MTColor circleColor;

	private MTEllipse displayShape;

	private ArrayList<PImage> cursorsImagesList;

	/**
	 * Instancie un nouveau traqueur
	 * 
	 * @param mtApp
	 *            L'application
	 * @param currentScene
	 *            La sc�ne courante
	 */
	public MyCursor(MTColor circleColor, String path, MTApplication mtApp,
			Iscene currentScene) {
		this.path = path;
		this.app = mtApp;
		this.scene = currentScene;
		this.circleColor = circleColor;
		this.cursorIDToDisplayShape = new HashMap<InputCursor, AbstractShape>();
		this.cursorsImagesList = new ArrayList();
		cursorsImagesList.add(app.loadImage(this.path + "standard.png"));
		cursorsImagesList.add(app.loadImage(this.path + "loading.png"));
		cursorsImagesList.add(app.loadImage(this.path
				+ "zoneSelectionnable.png"));
		// Permet de garder le curseur toujours visible, au plus haut niveau des
		// couches de composants
		// MTOverlayContainer overlay =
		// checkForExistingOverlay(scene.getCanvas());
		//
		this.overlayGroup = new MTOverlayContainer(app, "Cursor Trace group");
		this.app.invokeLater(new Runnable() {
			public void run() {
				scene.getCanvas().addChild(overlayGroup);
			}
		});
	}

	/**
	 * Cr�e le composant � afficher.
	 * 
	 * @param applet
	 *            L'applet
	 * @param position
	 *            La position
	 * 
	 * @return La forme abstraite
	 */
	protected AbstractShape createDisplayComponent(PApplet applet,
			Vector3D position) {
		displayShape = new CursorEllipse(applet, position, 30, 30);
		displayShape.setPickable(false);
		displayShape.setNoFill(true);
		displayShape.setDrawSmooth(true);
		displayShape.setStrokeWeight(2);
		displayShape.setStrokeColor(this.circleColor);
		setCursor(0);
		return displayShape;
	}

	public void setCursor(int index) {
		if (cursorsImagesList.size() >= index) {
			displayShape.setTexture(cursorsImagesList.get(index));
		}
	}

	private class CursorEllipse extends MTEllipse {
		public CursorEllipse(PApplet applet, Vector3D centerPoint,
				float radiusX, int segments) {
			super(applet, centerPoint, radiusX, radiusX, segments);
		}

		@Override
		protected IBoundingShape computeDefaultBounds() {
			return null;
		}

		@Override
		protected void setDefaultGestureActions() {
		}
	}

	@Override
	public void processInputEvtImpl(MTInputEvent inputEvent) {
		if (inputEvent instanceof AbstractCursorInputEvt) {
			AbstractCursorInputEvt cursorEvt = (AbstractCursorInputEvt) inputEvent;
			InputCursor c = ((AbstractCursorInputEvt) inputEvent).getCursor();
			Vector3D position = new Vector3D(cursorEvt.getPosX(),
					cursorEvt.getPosY(), 0);

			AbstractShape displayShape = null;
			switch (cursorEvt.getId()) {
			case AbstractCursorInputEvt.INPUT_DETECTED:
				displayShape = createDisplayComponent(app, position);
				cursorIDToDisplayShape.put(c, displayShape);
				overlayGroup.addChild(displayShape);
				displayShape.setPositionGlobal(position);
				break;
			case AbstractCursorInputEvt.INPUT_UPDATED:
				displayShape = cursorIDToDisplayShape.get(c);
				if (displayShape != null) {
					displayShape.setPositionGlobal(position);
				}
				break;
			case AbstractCursorInputEvt.INPUT_ENDED:
				displayShape = cursorIDToDisplayShape.remove(c);
				if (displayShape != null) {
					displayShape.destroy();
				}
				break;
			default:
				break;
			}
		}
	}
}
