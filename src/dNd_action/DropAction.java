package dNd_action;

import java.util.ArrayList;

import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

/**
 * Diff�rent du DragAndDropAction, l'impl�mentation du DropAction est activ�e
 * seulement en cas d'�v�nement DropTarget.componentDropped(MTComponent
 * droppedComponent). Cela signifie que DropAction demande beaucoup moins
 * d'effort en performance informatique car il ne recherche pas les zones de
 * d�pose � chaque fois qu'un objet est d�plac�. DropAction devrait �tre utilis�
 * � la place des DragAndDropAction, si le DragAndDropAction n'est pas assez
 * performant.
 * 
 * @author Uwe Laufs, arrang� par Robin Jespierre
 */
public class DropAction extends AbstractDnDAction {
	private boolean pickableObjectsOnly = false;
	private ArrayList<DropActionListener> listeners = new ArrayList<DropActionListener>();

	public DropAction() {
	}

	/**
	 * @param pickableObjectsOnly
	 *            Si param�tr� � "true", les objets avec isPickable() = 'false'
	 *            ne seront pas v�rifi�. Mettre ce param�tre � "false" autant
	 *            que possible afin d'am�liorer la performance
	 */
	public DropAction(boolean pickableObjectsOnly) {
		this.pickableObjectsOnly = pickableObjectsOnly;
	}

	@Override
	public boolean gestureDetected(MTGestureEvent g) {
		return false;
	}

	@Override
	public boolean gestureUpdated(MTGestureEvent g) {
		return false;
	}

	@Override
	public boolean gestureEnded(MTGestureEvent g) {
		if (!(g.getTargetComponent() instanceof MTComponent)) {
			return false;
		}
		DropTarget dropTarget = this.detectDropTarget(g,
				this.pickableObjectsOnly);
		if (g instanceof DragEvent) {
			DragEvent dragEvent = (DragEvent) g;

			if (dropTarget != null) {
				dropTarget.componentDropped(
						(MTComponent) g.getTargetComponent(), dragEvent);
				for (int i = 0; i < this.listeners.size(); i++) {
					this.listeners.get(i).objectDroppedOnTarget(
							(MTComponent) g.getTargetComponent(), dropTarget,
							dragEvent);
				}
			} else {
				for (int i = 0; i < this.listeners.size(); i++) {
					this.listeners.get(i).objectDroppedNotOnTarget(
							(MTComponent) g.getTargetComponent(), dragEvent);
				}
			}
		}

		return false;
	}

	public void addListener(DropActionListener dal) {
		if (!this.listeners.contains(dal)) {
			this.listeners.add(dal);
		}
		System.out.println("#listeners:" + this.listeners.size());
	}
}