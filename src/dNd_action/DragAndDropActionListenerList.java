package dNd_action;

import java.util.ArrayList;

import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

/**
 * <p>
 * Classe d'utilit� sp�cialement pour contenir des instances de
 * DragAndDropActionListener. Elle m�me un DragAndDropActionListener, elle
 * transf�re les appels � ces m�thodes "listeners" � tous ses membres.
 * </p>
 * 
 * @author R.Scarberry, arrang� par Robin Jespierre
 *
 */
public class DragAndDropActionListenerList extends
		ArrayList<DragAndDropActionListener> implements
		DragAndDropActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4853052488927421483L;

	@Override
	public void objectDroppedOnTarget(MTComponent droppedObject, DropTarget dt,
			DragEvent de) {
		if (this.size() > 0) {
			// N'utilise pas d'it�rator car un "listener" pourrait se supprimer
			// lui-m�me en r�ponse � l'�v�nement
			// et caus� une ConcurrentModificationException.
			DragAndDropActionListener[] listeners = this
					.toArray(new DragAndDropActionListener[this.size()]);
			for (DragAndDropActionListener l : listeners) {
				l.objectDroppedOnTarget(droppedObject, dt, de);
			}
		}
	}

	@Override
	public void objectDroppedNotOnTarget(MTComponent droppedObject, DragEvent de) {
		if (this.size() > 0) {
			// N'utilise pas d'it�rator car un "listener" pourrait se supprimer
			// lui-m�me en r�ponse � l'�v�nement
			// et caus� une ConcurrentModificationException.
			DragAndDropActionListener[] listeners = this
					.toArray(new DragAndDropActionListener[this.size()]);
			for (DragAndDropActionListener l : listeners) {
				l.objectDroppedNotOnTarget(droppedObject, de);
			}
		}
	}

	@Override
	public void objectEnteredTarget(MTComponent object, DropTarget dt,
			DragEvent de) {
		if (this.size() > 0) {
			// N'utilise pas d'it�rator car un "listener" pourrait se supprimer
			// lui-m�me en r�ponse � l'�v�nement
			// et caus� une ConcurrentModificationException.
			DragAndDropActionListener[] listeners = this
					.toArray(new DragAndDropActionListener[this.size()]);
			for (DragAndDropActionListener l : listeners) {
				l.objectEnteredTarget(object, dt, de);
			}
		}
	}

	@Override
	public void objectExitedTarget(MTComponent object, DropTarget dt,
			DragEvent de) {
		if (this.size() > 0) {
			// N'utilise pas d'it�rator car un "listener" pourrait se supprimer
			// lui-m�me en r�ponse � l'�v�nement
			// et caus� une ConcurrentModificationException.
			DragAndDropActionListener[] listeners = this
					.toArray(new DragAndDropActionListener[this.size()]);
			for (DragAndDropActionListener l : listeners) {
				l.objectExitedTarget(object, dt, de);
			}
		}
	}
}