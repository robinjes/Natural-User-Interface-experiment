package dNd_action;

import java.util.List;

import org.mt4j.components.MTCanvas;
import org.mt4j.components.MTComponent;
import org.mt4j.components.PickResult;
import org.mt4j.components.PickResult.PickEntry;
import org.mt4j.components.interfaces.IMTComponent3D;
import org.mt4j.components.visibleComponents.AbstractVisibleComponent;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputProcessors.IGestureEventListener;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.util.math.Vector3D;

/**
 * Classe reprise du projet
 * "Office Layout Enginering, Project Semantic Touch, (C) 2009 Fraunhofer IAO",
 * comment�e en francais, pour le projet "Interface NUI -TPB2014".
 * 
 * @author Uwe Laufs, arrang� par Robin Jespierre
 */
public abstract class AbstractDnDAction implements IGestureEventListener {
	/**
	 * Classe abstraite qui fourni des fonctionnalit�s requises pour les actions
	 * de gestuelles
	 * 
	 * @author Uwe Laufs, arrang� par Robin Jespierre
	 */
	@Override
	public boolean processGestureEvent(MTGestureEvent g) {
		if (g instanceof DragEvent) {
			switch (g.getId()) {
			case DragEvent.GESTURE_DETECTED:
				this.gestureDetected(g);
				break;
			case DragEvent.GESTURE_UPDATED:
				this.gestureUpdated(g);
				break;
			case DragEvent.GESTURE_ENDED:
				this.gestureEnded(g);
				break;
			}
		}
		return false;
	}

	protected MTCanvas getParentCanvas(MTComponent as) {
		MTComponent tmp = as.getRoot();
		if (tmp instanceof MTCanvas) {
			return (MTCanvas) tmp;
		} else {
			MTComponent mtc = as;
			while ((!(mtc == null))
					&& (!((mtc = mtc.getParent()) instanceof MTCanvas))) {
			}
			return (MTCanvas) mtc;
		}
	}

	/**
	 * @param g
	 * @return Les retours la position du curseur ou null, si il n'y a pas de
	 *         curseur
	 */
	protected Vector3D getCursorPosition(MTGestureEvent g) {
		if (g instanceof DragEvent) {
			DragEvent dragEvent = (DragEvent) g;
			InputCursor cursor = dragEvent.getDragCursor();
			AbstractCursorInputEvt lastCursorEvent = cursor.getEvents().get(
					cursor.getEventCount() - 1);
			return new Vector3D(lastCursorEvent.getPosX(),
					lastCursorEvent.getPosY());
		} else if (g instanceof ScaleEvent) {
			return null;
		} else {
			return null;
		}
	}

	protected MTComponent getVisibileParentComponent(MTGestureEvent g) {
		IMTComponent3D inputComponent = g.getTargetComponent();
		if ((inputComponent instanceof MTComponent)) {
			return getVisibileParentComponent((MTComponent) inputComponent);
		} else {
			return null;
		}
	}

	/**
	 * @param Un
	 *            MTComponent
	 * @return Le parent AbstractVisibleComponent au plus haut niveau dans la
	 *         hierarchie des components
	 */
	protected MTComponent getVisibileParentComponent(MTComponent sourceComponent) {
		MTComponent candidate = null;
		{
			MTComponent tmp = sourceComponent;
			while (!((tmp = tmp.getParent()) instanceof MTCanvas)) {
				if (tmp instanceof AbstractVisibleComponent) {
					candidate = (MTComponent) tmp;
				}
			}
		}
		if (candidate == null) {
			return sourceComponent;
		} else {
			return candidate;
		}
	}

	/**
	 * This method is called in case GESTURE_DETECTED.
	 * 
	 * @param g
	 *            Le MTGestureEvent courant
	 * @return boolean
	 */
	public abstract boolean gestureDetected(MTGestureEvent g);

	/**
	 * Cette m�thode est appel�e lorsque le geste est M�J (GESTURE_UPDATED).
	 * 
	 * @param g
	 *            Le MTGestureEvent courant
	 * @return boolean
	 */
	public abstract boolean gestureUpdated(MTGestureEvent g);

	/**
	 * This method is called in case GESTURE_ENDED.
	 * 
	 * @param g
	 *            Le MTGestureEvent courant
	 * @return boolean
	 */
	public abstract boolean gestureEnded(MTGestureEvent g);

	/**
	 * @param g
	 * @return Le MTDropTarget qui est s�lect� par l'�v�nement du geste courant
	 *         ou null si il ne se trouve pas sur une zone MTDropTarget
	 */
	protected DropTarget detectDropTarget(MTGestureEvent g,
			boolean pickableObjectsOnly) {
		DropTarget returnValue = null;
		MTComponent sourceComponent = (MTComponent) g.getTargetComponent();
		Vector3D componentDropPositions = this.getCursorPosition(g);
		{
			MTCanvas parentCanvas = this.getParentCanvas(sourceComponent);
			PickResult prCanvas = parentCanvas.pick(
					componentDropPositions.getX(),
					componentDropPositions.getY(), pickableObjectsOnly);
			List<PickEntry> pickList = prCanvas.getPickList();

			for (int i = 0; i < pickList.size(); i++) {
				PickEntry currentPickEntry = pickList.get(i);
				MTComponent currentComponent = currentPickEntry.hitObj;
				// get "parent" of composed components for listener invocation
				// currentComponent = this
				// .getVisibileParentComponent(currentComponent);
				// Ignore le composant source actuellement d�plac�
				if (!(currentComponent.equals(sourceComponent))) {
					if (currentComponent instanceof DropTarget) {
						returnValue = ((DropTarget) currentComponent);
						return returnValue;
					}
				}
			}
		}
		return null;
	}
}