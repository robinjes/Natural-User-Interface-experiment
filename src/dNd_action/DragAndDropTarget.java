package dNd_action;

import org.mt4j.components.MTComponent;

public interface DragAndDropTarget extends DropTarget {
        /**
         * Est appel� par DragAndDropAction quand un composant est d�plac� au dessus d'une zone de d�pose. 
         * @param enteredComponent Le MTComponent qui est entr� dans une zone de d�pose
         */
        public void componentEntered(MTComponent enteredComponent);
        /**
         * Est appel� par DragAndDropAction quand un composant est d�plac� en dehors d'une zone de d�pose. 
         * @param exitedComponent Le MTComponent qui est sorti d'une zone de d�pose
         */
        public void componentExited(MTComponent exitedComponent);
}