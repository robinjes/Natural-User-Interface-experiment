package dNd_action;

import java.util.Hashtable;

import org.mt4j.components.MTComponent;
import org.mt4j.components.interfaces.IMTComponent3D;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

/**
 * Lorsque le DragProcessor est "�cout�" DefaultDragAndDropAction appels le componentDropped quand un MTComponent est d�pos� au dessus des autres MTComponent qui impl�mentent MTDropTarget
 * 
 * @author Uwe Laufs, arrang� par Robin Jespierre
 * @see DropTarget
 */
public class DragAndDropAction extends AbstractDnDAction {
	private Hashtable<IMTComponent3D, DropTarget> componentAndCurrentTarget = new Hashtable<IMTComponent3D, DropTarget>();

	//Permet d'emmagasiner les "listeners"
	private DragAndDropActionListenerList listeners;

	private boolean pickableObjectsOnly = false;

	public DragAndDropAction() {

	}

	/**
	 * @param pickableObjectsOnly
	 * Si param�tr� � "true", les objets avec isPickable() = 'false' ne seront pas v�rifi�.
	 * Mettre ce param�tre � "false" autant que possible afin d'am�liorer la performance
	 */
	public DragAndDropAction(boolean pickableObjectsOnly) {
		this.pickableObjectsOnly = pickableObjectsOnly;
	}

	public void addDragAndDropActionListener(DragAndDropActionListener l) {
		if (listeners == null)
			listeners = new DragAndDropActionListenerList();
		if (!listeners.contains(l)) {
			listeners.add(l);
		}
	}

	public void removeDragAndDropActionListener(DragAndDropActionListener l) {
		if (listeners != null) {
			listeners.remove(l);
			if (listeners.size() == 0) {
				listeners = null;
			}
		}
	}

	@Override
	public boolean gestureDetected(MTGestureEvent g) {
		return false;
	}

	@Override
	public boolean gestureUpdated(MTGestureEvent g) {
		IMTComponent3D sourceComponent = g.getTargetComponent();
		if (!(sourceComponent instanceof MTComponent)) {
			return false;
		}

		MTComponent draggedComponent = (MTComponent) sourceComponent;
		DragEvent de = (g instanceof DragEvent) ? (DragEvent) g : null;

		DragAndDropTarget dropTarget = (DragAndDropTarget) this.detectDropTarget(g, pickableObjectsOnly);

		// Le composant se trouve-t-il sur une zone de d�pose?
		if (dropTarget != null && !targetRemembered(sourceComponent)) {
			this.storeComponentTarget(sourceComponent, dropTarget);
			dropTarget.componentEntered((MTComponent) sourceComponent);
			if (listeners != null) {
				listeners.objectEnteredTarget(draggedComponent, dropTarget, de);
			}
			// Le composant est-t-il d�plac� � un autre endroit?
		} else if (dropTarget == null && !targetRemembered(sourceComponent)) {
			// Ignore
		} else if (this.targetChanged(sourceComponent, dropTarget)) {
			DropTarget target = this.getComponentTarget(sourceComponent);
			if (target instanceof DragAndDropTarget) {
				((DragAndDropTarget) target)
						.componentExited((MTComponent) sourceComponent);
				this.forgetComponentTarget(sourceComponent);
				if (listeners != null) {
					listeners.objectExitedTarget(draggedComponent, target, de);
				}
				if (dropTarget != null) {
					this.storeComponentTarget(sourceComponent, dropTarget);
					dropTarget.componentEntered((MTComponent) sourceComponent);
					if (listeners != null) {
						listeners.objectEnteredTarget(draggedComponent,
								dropTarget, de);
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean gestureEnded(MTGestureEvent g) {
		IMTComponent3D target = g.getTargetComponent();
		if (!(target instanceof MTComponent)) {
			return false;
		}

		MTComponent droppedComponent = (MTComponent) target;

		if (g instanceof DragEvent) {

			DropTarget dropTarget = this.detectDropTarget(g,
					pickableObjectsOnly);

			DragEvent dragEvent = ((DragEvent) g);
			if (dropTarget != null) {
				dropTarget.componentDropped(
						(MTComponent) g.getTargetComponent(), dragEvent);
				this.storeComponentTarget(g.getTargetComponent(), dropTarget);
				if (listeners != null) {
					listeners.objectDroppedOnTarget(droppedComponent,
							dropTarget, dragEvent);
				}
			} else {
				if (listeners != null) {
					listeners.objectDroppedNotOnTarget(droppedComponent,
							dragEvent);
				}
			}
		}
		return false;
	}

	private void storeComponentTarget(IMTComponent3D component,
			DropTarget dropTarget) {
		if (component != null && dropTarget != null) {
			this.componentAndCurrentTarget.put(component, dropTarget);
		}
	}

	private void forgetComponentTarget(IMTComponent3D component) {
		if (component != null) {
			this.componentAndCurrentTarget.remove(component);
		}
	}

	private DropTarget getComponentTarget(IMTComponent3D component) {
		if (component != null) {
			return this.componentAndCurrentTarget.get(component);
		} else {
			return null;
		}
	}

	private boolean targetRemembered(IMTComponent3D component) {
		if (component == null) {
			return false;
		} else {
			return this.componentAndCurrentTarget.get(component) != null;
		}
	}

	private boolean targetChanged(IMTComponent3D component,
			DropTarget dropTarget) {
		DropTarget rememberedTarget = this.componentAndCurrentTarget
				.get(component);
		if (rememberedTarget == null && dropTarget == null) {
			return false;
		} else if (rememberedTarget != null && dropTarget != null) {
			return !rememberedTarget.equals(dropTarget);
		} else {
			return true;
		}
	}
}