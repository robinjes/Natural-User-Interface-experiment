package dNd_action;

import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

/**
 * @author Uwe Laufs, arrang� par Robin Jespierre
 * @see DragAndDropAction
 */
public interface DropTarget {

	/**
	 * Est appel� par DropAction ou DragAndDropAction lorsqu'un composant est
	 * d�pos� sur une zone de d�pose
	 * 
	 * @param droppedComponent
	 *            Le MTComponent qui est d�pos� sur une zone de d�pose.
	 */
	public void componentDropped(MTComponent droppedComponent, DragEvent de);

}