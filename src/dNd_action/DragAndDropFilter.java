package dNd_action;

import org.mt4j.components.MTComponent;

/**
 * <p>
 * Interface pour les filtres qui pourraient �tre appliqu� aux composants appel�s dans les op�rations de DnD.
 * </p>
 * 
 * @author R. Scarberry, arrang� par Robin Jespierre
 *
 */
public interface DragAndDropFilter {

	/**
	 * Retourne "true" si le composant sp�cifi� est acceptable pour une op�ration DnD.  
	 * 
	 * @param component
	 * 
	 * @return true ou false.
	 */
	boolean dndAccept(MTComponent component);

}