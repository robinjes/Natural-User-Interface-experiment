package dNd_action;

import org.mt4j.components.MTComponent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;

/**
 * Extends DropActionListener pour que les "listeners" puissent �tre modifi�s
 * quand un objet entre ou sort d'une zone de d�pose.
 * 
 * @author R. Scarberry, arrang� par Robin Jespierre
 *
 */
public interface DragAndDropActionListener extends DropActionListener {
	void objectEnteredTarget(MTComponent object, DropTarget dt, DragEvent de);

	void objectExitedTarget(MTComponent object, DropTarget dt, DragEvent de);
}