package dNd_action;

import java.util.ArrayList;

import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.components.visibleComponents.widgets.MTTextArea;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.util.MTColor;
import org.mt4j.util.font.FontManager;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.Vertex;

import processing.core.PApplet;

/**
 * 
 * @author Uwe Laufs, arrang� par Robin Jespierre
 *
 */
public class MyDragAndDropTarget extends MTPolygon implements DragAndDropTarget {

	private ArrayList<MTComponent> droppedComponents = new ArrayList<MTComponent>();
	private MTTextArea text;
	private float width;
	private float height;
	private float xposition;
	private float yposition;

	@SuppressWarnings("deprecation")
	public MyDragAndDropTarget(Vertex[] vertex, PApplet pApplet, IFont font) {
		super(vertex, pApplet);

		setXPosition(vertex[0].x);
		setYPosition(vertex[0].y);

		IFont arial = FontManager.getInstance().createFont(pApplet,
				"arial.ttf", 30, // Taille Font
				new MTColor(0, 0, 0, 255), // Couleur de remplissage de la Font
				new MTColor(0, 0, 0, 255));

		this.text = new MTTextArea(pApplet, arial);
		this.text.setNoFill(true);
		this.text.setNoStroke(true);
		this.setStrokeWeight(3f);
		this.addChild(text);
		this.text.setPositionGlobal(this.getCenterPointGlobal());
		this.text.removeAllGestureEventListeners();
		this.setPickable(false);
	}

	public void setText() {
		String text = this.getName() + "\n";
		for (int i = 0; i < this.droppedComponents.size(); i++) {
			text += "- " + droppedComponents.get(i).getName() + "\n";
		}
		this.text.setText(text);
		this.text.setPositionGlobal(this.getCenterPointGlobal());
	}

	private void setXPosition(float xposition) {
		this.xposition = xposition;
	}

	private float getXPosition() {
		return this.xposition;
	}

	private void setYPosition(float yposition) {
		this.yposition = yposition;
	}

	private float getYPosition() {
		return this.yposition;
	}

	@Override
	public void componentDropped(MTComponent droppedComponent, DragEvent de) {
		if (!droppedComponents.contains(droppedComponent)) {
			this.droppedComponents.add(droppedComponent);
		}

		this.setStrokeColor(this.getFillColor());
	}

	@Override
	public void componentEntered(MTComponent droppedComponent) {
		this.setStrokeColor(new MTColor(255, 0, 0));
	}

	@Override
	public void componentExited(MTComponent droppedComponent) {
		this.droppedComponents.remove(droppedComponent);
		this.setStrokeColor(this.getFillColor());
	}
}
